local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant      = 'Fell_Cleave',
    THMode          = true,
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Genbu\'s Kabuto',
        Body    = 'Shadow Brstplate',
        Hands   = 'Ftr. Mufflers +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Hermes\' Sandals',
        Neck    = 'Fortitude Torque',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Jelly Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Genbu\'s Kabuto',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Shadow Gauntlets',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Hermes\' Sandals',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_Default'] = {
        Head    = 'Walahra Turban',
        Body    = 'Adaman Hauberk',
        Hands   = 'Dusk Gloves',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Walahra Turban',
        Body    = 'Adaman Hauberk',
        Hands   = 'Dusk Gloves',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Ancient Torque',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Fell_Cleave'] = {
        Head    = 'Genbu\'s Kabuto',
        Body    = 'Adaman Hauberk',
        Hands   = 'Ftr. Mufflers +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Fortitude Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Jelly Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_MDT'] = {
        Head    = 'Genbu\'s Kabuto',
        Body    = 'Adaman Hauberk',
        Hands   = 'Ftr. Mufflers +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Fortitude Torque',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['WS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['MHWS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Aesir Mantle',
    },
    ['Requiescat'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Soil Gorget',
        Waist   = 'Salire Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Suppanomimi',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Aesir Mantle',
    },
    ['Resolution'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Hct. Leggings',
        Neck    = 'Soil Gorget',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Aesir Mantle',
    },
    ['Upheaval'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Nocturnus Mail',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Hecatomb Subligar',
        Feet    = 'Hct. Leggings',
        Neck    = 'Shadow Gorget',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Mercenary\'s Ring',
        Back    = 'Aesir Mantle',
    },
    ['Fell Cleave'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Alkyoneus\'s Brc.',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Cerberus Mantle',
    },
    ['Midshot'] = {
        Head    = 'Optical Hat',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Seiryu\'s Kote',
        Legs    = 'Dusk Trousers',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Peacock Amulet',
        Waist   = 'Ryl.Kgt. Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Merman\'s Ring',
        Ring2   = 'Coral Ring',
        Back    = 'Amemet Mantle +1',
    },
    ['Haste'] = {
        Head    = 'Walahra Turban',
        Hands   = 'Dusk Gloves',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Waist   = 'Speed Belt',
        Ear1    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Ear1    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 12;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /fc /lac fwd /fc');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /ws "Upheaval" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /ws "Fell Cleave" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Entropy" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Requiescat" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Resolution" <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^c');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
end

local th_timer = 0;
local target_index = nil;

local fc_timer = 0;
local sub = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        elseif (args[2] == 'fc') then
            settings.TPVariant = 'Fell_Cleave';
        elseif (args[2] == 'mdt') then
            if (settings.TPVariant ~= 'MDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'MDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/fc') then
        fc_timer = 35;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    end
end

local subjob;
local counter = 0;
local macroset = 0;

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 11');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'NIN') and 2 or
                    (player.SubJob == 'DNC') and 3 or
                    (player.SubJob == 'SAM') and 4 or
                    (player.SubJob == 'THF') and 5 or
                    (player.SubJob == 'BLU') and 6 or 2;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Feet', 'Hermes\' Sandals');
        end

        if (player.SubJob == 'DRG') then
            gFunc.Equip('Ear2', 'Wyvern Earring');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        common.RemoveSleep();
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
    end

    if (player.SubJob == 'NON') then
        fc_timer = 0;
    end
    if (fc_timer <= 40) then
        fc_timer = fc_timer + 1;
    end
    if (fc_timer == 39) then
        if (equipment.Sub ~= nil) then
            sub = equipment.Sub.Name;
        end
        gFunc.Equip('Sub', 'Remove');
        gFunc.Equip('Neck', 'Fortitude Torque');
        gFunc.Equip('Hands', 'Shadow Gauntlets');
    end
    if (fc_timer == 40) then
        if (sub ~= nil) then
            gFunc.Equip('Sub', sub);
            sub = nil;
        else
            gFunc.Equip('Sub', 'Axe Grip');
        end
        gFunc.Equip('Neck', 'Fortitude Torque');
        gFunc.Equip('Hands', 'Shadow Gauntlets');
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (action.Name == 'Warcry') then
        gFunc.Equip('Head', 'Warrior\'s Mask');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    gFunc.EquipSet('Precast');
end

profile.HandleMidcast = function()
    local player = gData.GetPlayer();

    gFunc.EquipSet('Haste');

    if (player.SubJob == 'DRG') then
        gFunc.Equip('Ear2', 'Wyvern Earring');
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    gFunc.EquipSet('Midshot');

    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
    ['Requiescat']      = 'Requiescat',
    ['Resolution']      = 'Resolution',
    ['Fell Cleave']     = 'Fell Cleave',
    ['Upheaval']        = 'Upheaval',
};

local gorget_map = {
    ['Requiescat']      = 'Soil Gorget',
    ['Resolution']      = 'Soil Gorget',
    ['Fell Cleave']     = 'Soil Gorget',
    ['Savage Blade']    = 'Soil Gorget',
    ['Vorpal Blade']    = 'Soil Gorget',
    ['Upheaval']        = 'Shadow Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();
    local environment = gData.GetEnvironment();

    gFunc.EquipSet('WS');

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end

    if (environment.Time >= 18 or environment.Time < 6 and action.Name ~= 'Requiescat') then
        gFunc.Equip('Ear2', 'Vampire Earring');
    end

    if (th_timer <= 20 and action.Name == 'Fell Cleave') then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end

    common.RemoveSleep();
end

return profile;
