local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant       = 'Default',
    TPThreshold     = 100,
    SaveMP          = false,
    THMode          = true,
};

local maxmp_bonus = {
    ['Idle_Default'] = {
        Head    = 14,
        Body    = 50,
        Hands   = 20,
        Legs    = 27,
        Feet    = 30,
        Neck    = 22,
        Waist   = 48,
        Ears    = 55, -- Both Ear1 and Ear2
        Ring1   = 30,
        Ring2   = 50,
        Back    = 25,
    },
    ['Idle_Alchemy'] = {
        Head    = 14,
        Hands   = 20,
        Legs    = 27,
        Feet    = 30,
        Waist   = 48,
        Ears    = 55, -- Both Ear1 and Ear2
        Ring1   = 50,
        Back    = 25,
    },
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Duelist\'s Chapeau',
        Body    = 'Dalmatica',
        Hands   = 'Blood Fng. Gnt.',
        Legs    = 'Blood Cuisses',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Beak Necklace +1',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Astral Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Succor Ring',
        Ring2   = 'Vivian Ring',
        Back    = 'Altruistic Cape',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Duelist\'s Chapeau',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Blood Fng. Gnt.',
        Legs    = 'Blood Cuisses',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Astral Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Vivian Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Altruistic Cape',
    },
    ['TP_Default'] = {
        Head    = 'Walahra Turban',
        Body    = 'Goliard Saio',
        Hands   = 'Wlk. Gloves +1',
        Legs    = 'Hydra Brais',
        Feet    = 'Dusk Ledelsens',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Optical Hat',
        Body    = 'Scorpion Harness',
        Hands   = 'Hydra Gloves',
        Legs    = 'Hydra Brais',
        Feet    = 'Hydra Gaiters',
        Neck    = 'Ancient Torque',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_BDT'] = {
        Head    = 'Walahra Turban',
        Body    = 'Blood Scale Mail',
        Hands   = 'Wlk. Gloves +1',
        Legs    = 'Hydra Brais',
        Feet    = 'Dusk Ledelsens',
        Neck    = 'Bloodbead Gorget',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['Resting'] = {
        Head    = 'Duelist\'s Chapeau',
        Body    = 'Wlk. Tabard +1',
        Hands   = 'Hydra Gloves',
        Legs    = 'Hydra Brais',
        Feet    = 'Hydra Gaiters',
        Neck    = 'Beak Necklace +1',
        Waist   = 'Duelist\'s Belt',
        Ear1    = 'Relaxing Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Star Ring',
        Ring2   = 'Vivian Ring',
        Back    = 'Altruistic Cape',
    },
    ['WS'] = {
        Head    = 'Voyager Sallet',
        Body    = 'Assault Jerkin',
        Hands   = 'Tarasque Mitts',
        Legs    = 'Dusk Trousers',
        Feet    = 'Rutter Sabatons',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Forager\'s Mantle',
    },
    ['MHWS'] = {
        Head    = 'Optical Hat',
        Body    = 'Assault Jerkin',
        Hands   = 'Tarasque Mitts',
        Legs    = 'Dusk Trousers',
        Feet    = 'Rutter Sabatons',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Forager\'s Mantle',
    },
    ['Requiescat'] = {
        Head    = 'Optical Hat',
        Body    = 'Blood Scale Mail',
        Hands   = 'Wlk. Gloves +1',
        Legs    = 'Jet Seraweels',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Soil Gorget',
        Waist   = 'Salire Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Suppanomimi',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Dew Silk Cape +1',
    },
    ['Haste'] = {
        Head    = 'Walahra Turban',
        Body    = 'Goliard Saio',
        Hands   = 'Wlk. Gloves +1',
        Legs    = 'Blood Cuisses',
        Feet    = 'Dusk Ledelsens',
        Waist   = 'Speed Belt',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Head    = 'Warlock\'s Chapeau',
        Body    = 'Duelist\'s Tabard',
        Legs    = 'Blood Cuisses',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['INT'] = {
        Head    = 'Errant Hat',
        Body    = 'Blood Scale Mail',
        Hands   = 'Errant Cuffs',
        Legs    = 'Jet Seraweels',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Lmg. Medallion +1',
        Waist   = 'Penitent\'s Rope',
        Ear1    = 'Abyssal Earring',
        Ear2    = 'Morion Earring +1',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Prism Cape',
    },
    ['MND'] = {
        Head    = 'Errant Hat',
        Body    = 'Blood Scale Mail',
        Hands   = 'Bricta\'s Cuffs',
        Legs    = 'Jet Seraweels',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Justice Badge',
        Waist   = 'Salire Belt',
        Ear1    = 'Celestial Earring',
        Ear2    = 'Harvest Earring',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Dew Silk Cape +1',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 11;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /tpt /lac fwd /tpt');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /smp /lac fwd /smp');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Savage Blade" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Requiescat" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Expiacion" <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
end

local th_timer = 0;
local target_index = nil;

local prev_set = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        elseif (args[2] == 'bdt') then
            if (settings.SetVariant ~= 'BDT') then
                if (string.match(settings.SetVariant, '[PMB]DT') == nil) then
                    prev_set = settings.SetVariant;
                end
                settings.SetVariant = 'BDT';
            elseif (prev_set) then
                settings.SetVariant = prev_set;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/tpt') then
        local number = tonumber(args[2]);

        if (number == nil or number < 0 or number > 3000) then
            gFunc.Error('Invalid argument: ' .. tostring(args[2]));
            return;
        else
            settings.TPThreshold = number;
        end
        gFunc.Message('TP threshold: ' .. args[2]);
    elseif (args[1] == '/smp') then
        settings.SaveMP = not settings.SaveMP;
        gFunc.Message('Save MP: ' .. tostring(settings.SaveMP));
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    end
end

local sub;
local main;
local restore_weapons = false;

local subjob;
local counter = 0;
local macroset = 1;

local function RestoreWeapons()
    if (restore_weapons) then
        gFunc.Equip('Main', main);
        gFunc.Equip('Sub', sub);
        restore_weapons = false;
    end
end

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 6');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'WHM') and 3 or
                    (player.SubJob == 'SCH') and 3 or
                    (player.SubJob == 'BLM') and 3 or 7;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Legs', 'Blood Cuisses');
            gFunc.Equip('Feet', 'Hydra Gaiters');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        RestoreWeapons();
    elseif (player.Status == 'Resting') then
        gFunc.EquipSet('Resting');

        if (player.TP <= settings.TPThreshold and restore_weapons == false) then
            main = equipment.Main and equipment.Main.Name or nil;
            sub = equipment.Sub and equipment.Sub.Name or nil;
            restore_weapons = true;

            gFunc.Equip('Main', 'Chatoyant Staff');
            gFunc.Equip('Sub', 'Dark Grip');
        end
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
        RestoreWeapons();
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    local action = gData.GetAction();

    gFunc.EquipSet('Precast');

    if (string.match(action.Name, 'Cur[ae]')) then
        gFunc.Equip('Feet', 'Zenith Pumps +1');
    end
end

local buff_spells = {
    'Refresh', 'Haste', 'Blink',
};

profile.HandleMidcast = function()
    local need_staff = true;
    local action = gData.GetAction();
    local player = gData.GetPlayer();
    local equipment = gData.GetEquipment();
    local environment = gData.GetEnvironment();

    for _, v in ipairs(buff_spells) do
        if (action.Name == v) then
            need_staff = false;
            break;
        end
    end

    if (need_staff and action.Element ~= nil and action.Element ~= 'Unknown'
        and string.find(action.Type, 'Magic', 1, true) ~= nil
        and player.TP <= settings.TPThreshold and restore_weapons == false) then
        main = equipment.Main and equipment.Main.Name or nil;
        sub = equipment.Sub and equipment.Sub.Name or nil;
        restore_weapons = true;

        gFunc.Equip('Main', common.staff_table[action.Element]);
        gFunc.Equip('Sub', action.Element .. ' Grip');
    end

    if (action.Type == 'Black Magic') then
        gFunc.EquipSet('INT');
    elseif (action.Type == 'White Magic') then
        gFunc.EquipSet('MND');
    else
        gFunc.EquipSet('Haste');
    end

    if (action.Skill == 'Healing Magic') then
        gFunc.Equip('Body', 'Duelist\'s Tabard');
        gFunc.Equip('Legs', 'Warlock\'s Tights');
        gFunc.Equip('Neck', 'Fylgja Torque +1');
        gFunc.Equip('Back', 'Dew Silk Cape +1');
    elseif (action.Skill == 'Enhancing Magic') then
        gFunc.Equip('Body', 'Goliard Saio');
        gFunc.Equip('Hands', 'Duelist\'s Gloves');
        gFunc.Equip('Legs', 'Warlock\'s Tights');
        gFunc.Equip('Feet', 'Shrewd Pumps');
        gFunc.Equip('Neck', 'Enhancing Torque');
        gFunc.Equip('Back', 'Merciful Cape');
    elseif (action.Skill == 'Enfeebling Magic') then
        gFunc.Equip('Head', 'Blood Mask');
        gFunc.Equip('Body', 'Wlk. Tabard +1');
        gFunc.Equip('Hands', 'Bricta\'s Cuffs');
        gFunc.Equip('Legs', 'Shadow Trews');
        gFunc.Equip('Waist', 'Salire Belt');
        gFunc.Equip('Ear1', 'Helenus\'s Earring');
        gFunc.Equip('Ear2', 'Cass. Earring');
        gFunc.Equip('Back', 'Altruistic Cape');
    elseif (action.Skill == 'Elemental Magic') then
        gFunc.Equip('Head', 'Warlock\'s Chapeau');
        gFunc.Equip('Hands', 'Bricta\'s Cuffs');
        gFunc.Equip('Legs', 'Shadow Trews');
        gFunc.Equip('Feet', 'Yigit Crackows');
        gFunc.Equip('Waist', 'Salire Belt');
        gFunc.Equip('Ear1', 'Helenus\'s Earring');
        gFunc.Equip('Ear2', 'Cass. Earring');
        gFunc.Equip('Back', 'Merciful Cape');
    elseif (action.Skill == 'Dark Magic') then
        gFunc.Equip('Head', 'Zenith Crown +1');
        gFunc.Equip('Hands', 'Blood Fng. Gnt.');
        gFunc.Equip('Waist', 'Salire Belt');
        gFunc.Equip('Ear1', 'Helenus\'s Earring');
        gFunc.Equip('Ear2', 'Cass. Earring');
        gFunc.Equip('Back', 'Merciful Cape');
    end

    common.EvaluateObi(action.Element);

    if (player.Status ~= 'Engaged') then
        common.SaveMP(sets['Idle_' .. settings.IdleVariant], maxmp_bonus['Idle_' .. settings.IdleVariant], settings.SaveMP);
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
    ['Requiescat']      = 'Requiescat',
    ['Vorpal Blade']    = 'MHWS',
    ['Flat Blade']      = 'MHWS',
    ['Exenterator']     = 'MHWS',
};

local gorget_map = {
    ['Requiescat']      = 'Soil Gorget',
    ['Savage Blade']    = 'Soil Gorget',
    ['Vorpal Blade']    = 'Soil Gorget',
    ['Exenterator']     = 'Soil Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();

    gFunc.EquipSet('WS');

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end
end

return profile;
