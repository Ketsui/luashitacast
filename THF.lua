local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant      = 'Default',
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Rog. Bonnet +1',
        Body    = 'Homam Corazza',
        Hands   = 'Rog. Armlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Trotter Boots',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Rog. Bonnet +1',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Rog. Armlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Trotter Boots',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Gathering'] = {
        Head    = 'Rog. Bonnet +1',
        Body    = 'Field Tunica',
        Hands   = 'Field Gloves',
        Legs    = 'Field Hose',
        Feet    = 'Field Boots',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_Default'] = {
        Head    = 'Rog. Bonnet +1',
        Body    = 'Rapparee Harness',
        Hands   = 'Homam Manopolas',
        Legs    = 'Homam Cosciales',
        Feet    = 'Homam Gambieras',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Rog. Bonnet +1',
        Body    = 'Homam Corazza',
        Hands   = 'Homam Manopolas',
        Legs    = 'Homam Cosciales',
        Feet    = 'Homam Gambieras',
        Neck    = 'Ancient Torque',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['WS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Hecatomb Harness',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Dusk Trousers',
        Feet    = 'Hct. Leggings',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Fierce Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Cerberus Mantle',
    },
    ['MHWS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Hecatomb Harness',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Dusk Trousers',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Cerberus Mantle',
    },
    ['Exenterator'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Homam Corazza',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Rogue\'s Culottes',
        Feet    = 'Rog. Poulaines +1',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Scouter\'s Rope',
        Ear1    = 'Drone Earring',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Emerald Ring',
        Ring2   = 'Emerald Ring',
        Back    = 'Aesir Mantle',
    },
    ['SA'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Homam Corazza',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Hecatomb Subligar',
        Feet    = 'Rog. Poulaines +1',
        Neck    = 'Ancient Torque',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Pixie Earring',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Spinel Ring',
        Back    = 'Aesir Mantle',
    },
    ['TA'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Homam Corazza',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Rogue\'s Culottes',
        Feet    = 'Rog. Poulaines +1',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Scouter\'s Rope',
        Ear1    = 'Drone Earring',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Emerald Ring',
        Ring2   = 'Emerald Ring',
        Back    = 'Aesir Mantle',
    },
    ['SATA'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Homam Corazza',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Hecatomb Subligar',
        Feet    = 'Rog. Poulaines +1',
        Neck    = 'Ancient Torque',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Spinel Ring',
        Back    = 'Aesir Mantle',
    },
    ['Haste'] = {
        Head    = 'Acubens Helm',
        Body    = 'Rapparee Harness',
        Hands   = 'Rog. Armlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Homam Gambieras',
        Neck    = 'Tiercel Necklace',
        Waist   = 'Speed Belt',
        Ear1    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Legs    = 'Homam Cosciales',
        Ear1    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Midshot'] = {
        Head    = 'Optical Hat',
        Body    = 'Pln. Khazagand',
        Hands   = 'Pln. Dastanas',
        Legs    = 'Dusk Trousers',
        Feet    = 'Homam Gambieras',
        Neck    = 'Peacock Amulet',
        Waist   = 'Scouter\'s Rope',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Merman\'s Ring',
        Ring2   = 'Coral Ring',
        Back    = 'Assassin\'s Cape',
    },
    ['Steal'] = {
        Head    = 'Rog. Bonnet +1',
        Hands   = 'Rog. Armlets +1',
        Legs    = 'Assassin\'s Culottes',
        Feet    = 'Rog. Poulaines +1',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 5;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^z /iset gat');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /item Pickaxe <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /ws "Exenterator" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^b /dismount');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Dancing Edge" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Shark Bite" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Evisceration" <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^z');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^c');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^b');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
end

local th_timer = 0;
local target_index = nil;

local prev_iset = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        elseif (args[2] == 'gat') then
            if (settings.IdleVariant ~= 'Gathering') then
                prev_iset = settings.IdleVariant;
                settings.IdleVariant = 'Gathering';
            elseif (prev_iset) then
                settings.IdleVariant = prev_iset;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/th') then
        th_timer = 0;
    end
end

local EquipRogueArmlets = function()
    if (gData.GetBuffCount('Trick Attack') > 0) then
        gFunc.Equip('Hands', 'Rog. Armlets +1');
    end
end

local subjob;
local counter = 0;
local macroset = 0;

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 16');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'NIN') and 2 or
                    (player.SubJob == 'DNC') and 3 or 1;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Feet', 'Trotter Boots');
        end

        if (th_timer <= 20) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
            gFunc.Equip('Hands', 'Rog. Armlets +1');
        end

        common.RemoveSleep();
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
    end

    EquipRogueArmlets();
    common.CheckManualEquips();
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (action.Name == 'Steal') then
        gFunc.EquipSet('Steal');
    elseif (action.Name == 'Mug') then
        gFunc.Equip('Head', 'Asn. Bonnet +1');
    elseif (action.Name == 'Trick Attack') then
        gFunc.Equip('Hands', 'Rog. Armlets +1');
    elseif (action.Name == 'Flee') then
        gFunc.Equip('Feet', 'Rog. Poulaines +1');
    end

    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        gFunc.Equip('Hands', 'Rog. Armlets +1');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    gFunc.EquipSet('Precast');
end

profile.HandleMidcast = function()
    local action_target = gData.GetActionTarget();

    gFunc.EquipSet('Haste');

    if (action_target ~= nil and action_target.Type == 'Monster') then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        gFunc.Equip('Hands', 'Rog. Armlets +1');
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    gFunc.EquipSet('Midshot');

    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        gFunc.Equip('Hands', 'Rog. Armlets +1');
    end
end

local ws_map = {
    ['Dancing Edge']    = 'MHWS',
    ['Evisceration']    = 'MHWS',
    ['Flat Blade']      = 'MHWS',
    ['Exenterator']     = 'Exenterator',
};

local gorget_map = {
    ['Exenterator']     = 'Soil Gorget',
    ['Evisceration']    = 'Soil Gorget',
    ['Dancing Edge']    = 'Soil Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();
    local player = gData.GetPlayer();

    gFunc.EquipSet('WS');

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gData.GetBuffCount('Sneak Attack') > 0 and gData.GetBuffCount('Trick Attack') > 0) then
        gFunc.EquipSet('SATA');
    elseif (gData.GetBuffCount('Sneak Attack') > 0) then
        gFunc.EquipSet('SA');
    elseif (gData.GetBuffCount('Trick Attack') > 0) then
        gFunc.EquipSet('TA');
    end

    if (gData.GetBuffCount('Trick Attack') > 0 and player.SubJob == 'NIN') then
        gFunc.Equip('Ear1', 'Genin Earring');
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end

    EquipRogueArmlets();
    common.RemoveSleep();
end

return profile;
