local common = {};

local manual_equip_table = {
    ['Sprout Beret']            = 'Head',
    ['Reraise Hairpin']         = 'Head',
    ['Mag. Bazubands +1']       = 'Hands',
    ['Warp Ring']               = 'Ring2',
    ['Echad Ring']              = 'Ring2',
    ['Empress Band']            = 'Ring2',
    ['Ecphoria Ring']           = 'Ring2',
    ['Shaper\'s Shawl']         = 'Back',
};

common.CheckManualEquips = function()
    local equipment = gData.GetEquipment();

    for k, v in pairs(manual_equip_table) do
        if (equipment[v] ~= nil and equipment[v]['Name'] == k) then
            gFunc.Equip(v, k);
        end
    end
end

common.RemoveSleep = function()
    local player = gData.GetPlayer();

    if ((gData.GetBuffCount(2) > 0 or gData.GetBuffCount(19) > 0) -- Sleep and Sleep II
        and player.HPP > 49
        and gData.GetBuffCount(3) == 0 and gData.GetBuffCount(128) == 0 and gData.GetBuffCount(129) == 0 -- a bunch of damage over time debuffs
        and gData.GetBuffCount(130) == 0 and gData.GetBuffCount(131) == 0 and gData.GetBuffCount(131) == 0
        and gData.GetBuffCount(132) == 0 and gData.GetBuffCount(133) == 0 and gData.GetBuffCount(134) == 0
        and gData.GetBuffCount(135) == 0 and gData.GetBuffCount(192) == 0) then
        if (player.MainJob == 'WAR' or player.MainJob == 'PLD') then
            gFunc.Equip('Neck', 'Berserker\'s Torque');
        else
            gFunc.Equip('Head', 'Frenzy Sallet');
        end
    end
end

common.staff_table = {
    ['Fire']            = 'Chatoyant Staff',
    ['Ice']             = 'Chatoyant Staff',
    ['Wind']            = 'Chatoyant Staff',
    ['Earth']           = 'Chatoyant Staff',
    ['Thunder']         = 'Chatoyant Staff',
    ['Water']           = 'Chatoyant Staff',
    ['Light']           = 'Chatoyant Staff',
    ['Dark']            = 'Chatoyant Staff',
    ['Non-Elemental']   = 'Chatoyant Staff',
};

local obi_table = {
    ['Fire']            = 'Karin Obi',
    ['Ice']             = 'Hyorin Obi',
    ['Wind']            = 'Furin Obi',
    ['Thunder']         = 'Rairin Obi',
    ['Light']           = 'Korin Obi',
    ['Dark']            = 'Anrin Obi',
};

local element_table = {
    ['Fire']            = 'Water',
    ['Ice']             = 'Fire',
    ['Wind']            = 'Ice',
    ['Earth']           = 'Wind',
    ['Thunder']         = 'Earth',
    ['Water']           = 'Thunder',
    ['Light']           = 'Dark',
    ['Dark']            = 'Light',
};

common.EvaluateObi = function(action_element)
    local multiplier = 1.0;
    local obi = obi_table[action_element];
    local environment = gData.GetEnvironment();
    local opposite_element = element_table[action_element];

    if (obi == nil) then
        return;
    end

    if (environment.WeatherElement == action_element) then
        if (string.match(environment.Weather, 'x2')) then
            multiplier = multiplier + 0.25;
        else
            multiplier = multiplier + 0.1;
        end
    elseif (environment.WeatherElement == opposite_element) then
        if (string.match(environment.Weather, 'x2')) then
            multiplier = multiplier - 0.25;
        else
            multiplier = multiplier - 0.1;
        end
    end

    if (environment.DayElement == action_element) then
        multiplier = multiplier + 0.1;
    elseif (environment.DayElement == opposite_element) then
        multiplier = multiplier - 0.1;
    end

    if (multiplier > 1.0 and obi ~= nil) then
        gFunc.Equip('Waist', obi);
    end
end

common.SaveMP = function(idle_set, idle_maxmp_bonus, save_mp)
    local maxmp_bonus = 0;
    local player = gData.GetPlayer();
    local target = gData.GetActionTarget();

    if (save_mp == false and target.Type == 'Monster') then
        return;
    end

    for k, v in pairs(idle_maxmp_bonus) do
        maxmp_bonus = maxmp_bonus + v;
        if (player.MP > player.MaxMP - maxmp_bonus) then
            if (k == 'Ears') then
                gFunc.Equip('Ear1', idle_set[Ear1]);
                gFunc.Equip('Ear2', idle_set[Ear2]);
            else
                gFunc.Equip(k, idle_set[k]);
            end
        end
    end
end

return common;
