local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant      = 'Default',
    THMode          = false,
    LACEnabled      = true,
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Blood Mask',
        Body    = 'Futhark Coat',
        Hands   = 'Runeist Mitons',
        Legs    = 'Blood Cuisses',
        Feet    = 'Hermes\' Sandals',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Blood Mask',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Runeist Mitons',
        Legs    = 'Blood Cuisses',
        Feet    = 'Hermes\' Sandals',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_Default'] = {
        Head    = 'Bahamut\'s Mask',
        Body    = 'Homam Corazza',
        Hands   = 'Homam Manopolas',
        Legs    = 'Homam Cosciales',
        Feet    = 'Futhark Boots',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Pixie Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Bahamut Mask',
        Body    = 'Homam Corazza',
        Hands   = 'Homam Manopolas',
        Legs    = 'Homam Cosciales',
        Feet    = 'Futhark Boots',
        Neck    = 'Ancient Torque',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Pixie Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_PDT'] = {
        Head    = 'Runeist Bandeau',
        Body    = 'Futhark Coat',
        Hands   = 'Denali Wristbands',
        Legs    = 'Runeist Trousers',
        Feet    = 'Dst. Leggings +1',
        Neck    = 'Repelling Collar',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Jelly Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_MDT'] = {
        Head    = 'Gavial Mask',
        Body    = 'Futhark Coat',
        Hands   = 'Denali Wristbands',
        Legs    = 'Runeist Trousers',
        Feet    = 'Runeist Bottes',
        Neck    = 'Bloodbead Gorget',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Shadow Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['TP_BDT'] = {
        Head    = 'Gavial Mask',
        Body    = 'Blood Scale Mail',
        Hands   = 'Denali Wristbands',
        Legs    = 'Bahamut\'s Hose',
        Feet    = 'Runeist Bottes',
        Neck    = 'Bloodbead Gorget',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Shadow Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['WS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Hecatomb Harness',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Runeist Trousers',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Fierce Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Cerberus Mantle',
    },
    ['MHWS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Hecatomb Harness',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Runeist Trousers',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Cerberus Mantle',
    },
    ['Requiescat'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Blood Scale Mail',
        Hands   = 'Denali Wristbands',
        Legs    = 'Jet Seraweels',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Soil Gorget',
        Waist   = 'Salire Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Celestial Earring',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Cerberus Mantle',
    },
    ['Stoneskin'] = {
        Head    = 'Errant Hat',
        Body    = 'Blood Scale Mail',
        Hands   = 'Denali Wristbands',
        Legs    = 'Jet Seraweels',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Justice Badge',
        Waist   = 'Salire Belt',
        Ear1    = 'Celestial Earring',
        Ear2    = 'Harvest Earring',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Merciful Cape',
    },
    ['Enmity'] = {
        Head    = 'Bahamut\'s Mask',
        Hands   = 'Homam Manopolas',
        Legs    = 'Runeist Trousers',
        Feet    = 'Runeist Bottes',
        Neck    = 'Harmonia\'s Torque',
        Waist   = 'Trance Belt',
        Ring1   = 'Mermaid Ring',
        Ear1    = 'Eris\' Earring',
        Ear2    = 'Eris\' Earring',
        Back    = 'Cerberus Mantle',
    },
    ['Haste'] = {
        Head    = 'Acubens Helm',
        Body    = 'Blood Scale Mail',
        Hands   = 'Homam Manopolas',
        Legs    = 'Futhark Trousers',
        Feet    = 'Futhark Boots',
        Neck    = 'Tiercel Necklace',
        Waist   = 'Speed Belt',
        Ear1    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Legs    = 'Futhark Trousers',
        Ear1    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Accuracy'] = {
        Head    = 'Optical Hat',
        Body    = 'Scorpion Harness',
        Hands   = 'Homam Manopolas',
        Legs    = 'Futhark Trousers',
        Feet    = 'Homam Gambieras',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Minuet Earring',
        Ring1   = 'Sniper\'s Ring',
        Ring2   = 'Sniper\'s Ring +1',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 14;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /toggle /lac fwd /toggle');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^z /set bdt');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /set pdt');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /set mdt');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Requiescat" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Resolution" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^b /toggle');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^z');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^b');
end

local th_timer = 0;
local target_index = nil;

local prev_set = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        elseif (args[2] == 'pdt') then
            if (settings.TPVariant ~= 'PDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'PDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        elseif (args[2] == 'mdt') then
            if (settings.TPVariant ~= 'MDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'MDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        elseif (args[2] == 'bdt') then
            if (settings.TPVariant ~= 'BDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'BDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    elseif (args[1] == '/toggle') then
        if (settings.LACEnabled) then
            AshitaCore:GetChatManager():QueueCommand(-1, '/lac disable');
        else
            AshitaCore:GetChatManager():QueueCommand(-1, '/lac enable');
        end
        settings.LACEnabled = not settings.LACEnabled;
    end
end

local subjob;
local counter = 0;
local macroset = 0;

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 17');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = 1;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Legs', 'Blood Cuisses');
            gFunc.Equip('Feet', 'Hermes\' Sandals');
        end

        if (player.SubJob == 'DRG') then
            gFunc.Equip('Ear2', 'Wyvern Earring');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        if (equipment.Main ~= nil and equipment.Main.Name ~= 'Galatyn'
            and string.match(settings.TPVariant, '[PMB]DT') == nil) then
            gFunc.Equip('Ear2', 'Suppanomimi');
        end

        common.RemoveSleep();
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (action.Name == 'Battuta') then
        gFunc.Equip('Head', 'Futhark Bandeau');
    elseif (action.Name == 'Swordplay') then
        gFunc.Equip('Hands', 'Futhark Mitons');
    elseif (action.Name == 'Gambit') then
        gFunc.Equip('Hands', 'Runeist Mitons');
    elseif (action.Name == 'Valiance' or action.Name == 'Vallation') then
        gFunc.Equip('Body', 'Runeist Coat');
    elseif (action.Name == 'Pflug') then
        gFunc.Equip('Feet', 'Runeist Bottes');
    elseif (action.Name == 'Swipe' or action.Name == 'Lunge') then
        gFunc.EquipSet('Enmity');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    if (gData.GetBuffCount('Hasso') > 0 or gData.GetBuffCount('Seigan') > 0) then
        gSettings.SpellOffset = 4.0;
    else
        gSettings.SpellOffset = 3.0;
    end

    gFunc.EquipSet('Precast');
end

local spell_table = {
    ['Flash']       = 'Enmity',
    ['Foil']        = 'Enmity',
    ['Stoneskin']   = 'Stoneskin',
};

profile.HandleMidcast = function()
    local player = gData.GetPlayer();
    local action = gData.GetAction();

    gFunc.EquipSet('Haste');

    if (player.SubJob == 'DRG') then
        gFunc.Equip('Ear2', 'Wyvern Earring');
    end

    if (spell_table[action.Name] ~= nil) then
        gFunc.EquipSet(spell_table[action.Name]);
    end

    if (action.Skill == 'Enhancing Magic') then
        gFunc.Equip('Neck', 'Enhancing Torque');
        gFunc.Equip('Back', 'Merciful Cape');
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
    ['Requiescat']      = 'Requiescat',
    ['Resolution']      = 'MHWS',
};

local gorget_map = {
    ['Requiescat']      = 'Soil Gorget',
    ['Resolution']      = 'Soil Gorget',
    ['Savage Blade']    = 'Soil Gorget',
    ['Vorpal Blade']    = 'Soil Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();
    local environment = gData.GetEnvironment();

    gFunc.EquipSet('WS');

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end

    common.RemoveSleep();
end

return profile;
