local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant       = 'Default',
    TPThreshold     = 100,
    SaveMP          = true,
    THMode          = true,
};

local maxmp_bonus = {
    ['Idle_Default'] = {
        Head    = 22,
        Body    = 20,
        Hands   = 20,
        Legs    = 27,
        Feet    = 31,
        Waist   = 48,
        Ears    = 55, -- Both Ear1 and Ear2
        Back    = 25,
    },
    ['Idle_Alchemy'] = {
        Head    = 22,
        Hands   = 20,
        Legs    = 27,
        Feet    = 31,
        Waist   = 48,
        Ears    = 55, -- Both Ear1 and Ear2
        Back    = 25,
    },
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Blood Mask',
        Body    = 'Mirage Jubbah',
        Hands   = 'Homam Manopolas',
        Legs    = 'Blood Cuisses',
        Feet    = 'Homam Gambieras',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Astral Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Merciful Cape',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Blood Mask',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Homam Manopolas',
        Legs    = 'Blood Cuisses',
        Feet    = 'Homam Gambieras',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Astral Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Merciful Cape',
    },
    ['TP_Default'] = {
        Head    = 'Walahra Turban',
        Body    = 'Blood Scale Mail',
        Hands   = 'Mag. Bazubands +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Homam Gambieras',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Refresh'] = {
        Head    = 'Walahra Turban',
        Body    = 'Mirage Jubbah',
        Hands   = 'Mag. Bazubands +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Homam Gambieras',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Mirage Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Optical Hat',
        Body    = 'Mirage Jubbah',
        Hands   = 'Homam Manopolas',
        Legs    = 'Homam Cosciales',
        Feet    = 'Homam Gambieras',
        Neck    = 'Ancient Torque',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Mirage Mantle',
    },
    ['Resting'] = {
        Head    = 'Walahra Turban',
        Body    = 'Mirage Jubbah',
        Hands   = 'Mag. Bazubands +1',
        Legs    = 'Mirage Shalwar',
        Feet    = 'Homam Gambieras',
        Neck    = 'Beak Necklace +1',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Relaxing Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Star Ring',
        Ring2   = 'Vivian Ring',
        Back    = 'Merciful Cape',
    },
    ['WS'] = {
        Head    = 'Voyager Sallet',
        Body    = 'Assault Jerkin',
        Hands   = 'Tarasque Mitts',
        Legs    = 'Dusk Trousers',
        Feet    = 'Rutter Sabatons',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Fierce Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['MHWS'] = {
        Head    = 'Optical Hat',
        Body    = 'Assault Jerkin',
        Hands   = 'Tarasque Mitts',
        Legs    = 'Dusk Trousers',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Brutal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Mirage Mantle',
    },
    ['Expiacion'] = {
        Head    = 'Voyager Sallet',
        Body    = 'Assault Jerkin',
        Hands   = 'Mag. Bazubands +1',
        Legs    = 'Dusk Trousers',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Soil Gorget',
        Waist   = 'Fierce Belt',
        Ear1    = 'Suppanomimi',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['Requiescat'] = {
        Head    = 'Optical Hat',
        Body    = 'Blood Scale Mail',
        Hands   = 'Mag. Bazubands +1',
        Legs    = 'Jet Seraweels',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Soil Gorget',
        Waist   = 'Salire Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Celestial Earring',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Dew Silk Cape +1',
    },
    ['Haste'] = {
        Head    = 'Acubens Helm',
        Body    = 'Blood Scale Mail',
        Hands   = 'Mag. Bazubands +1',
        Legs    = 'Blood Cuisses',
        Feet    = 'Homam Gambieras',
        Neck    = 'Tiercel Necklace',
        Waist   = 'Speed Belt',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Legs    = 'Blood Cuisses',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['STR_DEX'] = {
        Head    = 'Voyager Sallet',
        Body    = 'Magus Jubbah +1',
        Hands   = 'Enkidu\'s Mittens',
        Legs    = 'Mirage Shalwar',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Kubira Beads',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Minuet Earring',
        Ear2    = 'Harvest Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Smilod. Mantle +1',
    },
    ['DEX'] = {
        Head    = 'Voyager Sallet',
        Body    = 'Magus Jubbah +1',
        Hands   = 'Mirage Bazubands',
        Legs    = 'Mirage Shalwar',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Kubira Beads',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Minuet Earring',
        Ear2    = 'Harvest Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Smilod. Mantle +1',
    },
    ['STR_VIT'] = {
        Head    = 'Mirage Keffiyeh',
        Body    = 'Magus Jubbah +1',
        Hands   = 'Alkyoneus\'s Brc.',
        Legs    = 'Mirage Shalwar',
        Feet    = 'Rutter Sabatons',
        Neck    = 'Kubira Beads',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Minuet Earring',
        Ear2    = 'Heims Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Smilod. Mantle +1',
    },
    ['VIT'] = {
        Head    = 'Mirage Keffiyeh',
        Body    = 'Magus Jubbah +1',
        Hands   = 'Light Gauntlets',
        Legs    = 'Magus Shalwar',
        Feet    = 'Rutter Sabatons',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Minuet Earring',
        Ear2    = 'Heims Earring',
        Ring1   = 'Sniper\'s Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Smilod. Mantle +1',
    },
    ['STR_MND'] = {
        Head    = 'Mirage Keffiyeh',
        Body    = 'Magus Jubbah +1',
        Hands   = 'Alkyoneus\'s Brc.',
        Legs    = 'Jet Seraweels',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Kubira Beads',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Bushinomimi',
        Ear2    = 'Celestial Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Smilod. Mantle +1',
    },
    ['INT'] = {
        Head    = 'Errant Hat',
        Body    = 'Blood Scale Mail',
        Hands   = 'Errant Cuffs',
        Legs    = 'Jet Seraweels',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Lmg. Medallion +1',
        Waist   = 'Penitent\'s Rope',
        Ear1    = 'Abyssal Earring',
        Ear2    = 'Morion Earring +1',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Mirage Mantle',
    },
    ['INT_ACC'] = {
        Head    = 'Optical Hat',
        Body    = 'Magus Jubbah +1',
        Hands   = 'Errant Cuffs',
        Legs    = 'Mirage Shalwar',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Lmg. Medallion +1',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Abyssal Earring',
        Ear2    = 'Morion Earring +1',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Mirage Mantle',
    },
    ['MND'] = {
        Head    = 'Errant Hat',
        Body    = 'Blood Scale Mail',
        Hands   = 'Mirage Bazubands',
        Legs    = 'Jet Seraweels',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Justice Badge',
        Waist   = 'Salire Belt',
        Ear1    = 'Celestial Earring',
        Ear2    = 'Harvest Earring',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Dew Silk Cape +1',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 4;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /tpt /lac fwd /tpt');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /smp /lac fwd /smp');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^z /ma "Zephyr Mantle" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /ma "Metallic Body" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /ma "Refueling" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^b /ma "Triumphant Roar" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Savage Blade" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Requiescat" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Expiacion" <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^z');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^c');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^b');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
end

local th_timer = 0;
local target_index = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'ref') then
            settings.TPVariant = 'Refresh';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/tpt') then
        local number = tonumber(args[2]);

        if (number == nil or number < 0 or number > 3000) then
            gFunc.Error('Invalid argument: ' .. args[2]);
            return;
        else
            settings.TPThreshold = number;
        end
        gFunc.Message('TP threshold: ' .. args[2]);
    elseif (args[1] == '/smp') then
        settings.SaveMP = not settings.SaveMP;
        gFunc.Message('Save MP: ' .. tostring(settings.SaveMP));
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    end
end

local sub;
local main;
local restore_weapons = false;

local subjob;
local counter = 0;
local macroset = 1;

local function RestoreWeapons()
    if (restore_weapons) then
        gFunc.Equip('Main', main);
        gFunc.Equip('Sub', sub);
        restore_weapons = false;
    end
end

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 4');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'SAM') and 2 or
                    (player.SubJob == 'SCH') and 3 or
                    (player.SubJob == 'THF') and 5 or
                    (player.SubJob == 'WAR') and 6 or
                    (player.SubJob == 'DNC') and 7 or
                    (player.SubJob == 'NIN') and 8 or 1;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Legs', 'Blood Cuisses');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        RestoreWeapons();
    elseif (player.Status == 'Resting') then
        gFunc.EquipSet('Resting');

        if (player.TP <= settings.TPThreshold and restore_weapons == false) then
            main = equipment.Main and equipment.Main.Name or nil;
            sub = equipment.Sub and equipment.Sub.Name or nil;
            restore_weapons = true;

            gFunc.Equip('Main', 'Chatoyant Staff');
            gFunc.Equip('Sub', 'Dark Grip');
        end
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
        RestoreWeapons();
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    gFunc.EquipSet('Precast');
end

local song_roll_favor = {
    192, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 205, 206, 207, 209, 210, 214, 215, 216,
    217, 218, 219, 220, 221, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323,
    324, 325, 326, 327, 328, 329, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431,
};

local buff_spells = {
    'Cocoon', 'Refueling', 'Zephyr Mantle', 'Warm-Up', 'Metallic Body', 'Diamondhide',
    'Saline Coat', 'Plasma Charge', 'Memento Mori', 'Amplification', 'Triumphant Roar',
    'Exuviation',
};

local spell_map = {
    ['Head Butt']           = 'INT_ACC',
    ['Sub-zero Smash']      = 'INT_ACC',
    ['Seedspray']           = 'INT_ACC',
    ['Frenetic Rip']        = 'STR_DEX',
    ['Asuran Claws']        = 'STR_DEX',
    ['Disseverment']        = 'STR_DEX',
    ['Hysteric Barrage']    = 'DEX',
    ['Cannonball']          = 'STR_VIT',
    ['Body Slam']           = 'VIT',
    ['Frypan']              = 'STR_MND',
    ['Blitzstrahl']         = 'INT',
    ['Firespit']            = 'INT',
    ['Maelstrom']           = 'INT',
    ['Regurgitation']       = 'INT',
    ['Sheep Song']          = 'INT',
    ['Soporific']           = 'INT',
    ['Yawn']                = 'INT',
    ['MP Drainkiss']        = 'INT',
    ['Filamented Hold']     = 'INT',
    ['Blastbomb']           = 'INT',
    ['Actinic Burst']       = 'INT',
    ['Chaotic Eye']         = 'INT',
    ['Jettatura']           = 'INT',
    ['Temporal Shift']      = 'INT',
    ['Ice Break']           = 'INT',
    ['Frightful Roar']      = 'INT',
    ['Enervation']          = 'INT',
    ['Magic Fruit']         = 'MND',
    ['Wild Carrot']         = 'MND',
    ['Magic Hammer']        = 'MND',
    ['Mind Blast']          = 'MND',
    ['Healing Breeze']      = 'MND',
};

profile.HandleMidcast = function()
    local need_macc = false;
    local need_staff = true;
    local action = gData.GetAction();
    local player = gData.GetPlayer();
    local equipment = gData.GetEquipment();
    local target = gData.GetActionTarget();

    for _, v in ipairs(buff_spells) do
        if (action.Name == v) then
            need_staff = false;
            break;
        end
    end

    if (need_staff and action.Element ~= nil and action.Element ~= 'Unknown'
        and (action.Element ~= 'Non-Elemental' or action.Name == 'Wild Carrot'
        or action.Name == 'Magic Fruit' or action.Name == 'Healing Breeze')
        and string.find(action.Type, 'Magic', 1, true) ~= nil
        and player.TP <= settings.TPThreshold and restore_weapons == false) then
        main = equipment.Main and equipment.Main.Name or nil;
        sub = equipment.Sub and equipment.Sub.Name or nil;
        restore_weapons = true;

        gFunc.Equip('Main', common.staff_table[action.Element]);
        gFunc.Equip('Sub', action.Element .. ' Grip');
    end

    if (action.Type == 'Black Magic') then
        gFunc.EquipSet('INT');
    elseif (action.Type == 'White Magic') then
        gFunc.EquipSet('MND');
    else
        gFunc.EquipSet('Haste');

        if (spell_map[action.Name] ~= nil) then
            gFunc.EquipSet(spell_map[action.Name]);

            if (target ~= nil and target.Type == 'Monster'
                and (string.find(spell_map[action.Name], 'INT', 1, true) ~= nil
                or string.find(spell_map[action.Name], 'MND', 1, true) ~= nil)) then
                need_macc = true;
            end

            if (string.find(spell_map[action.Name], 'VIT', 1, true)) then
                for _, v in ipairs(song_roll_favor) do
                    if (gData.GetBuffCount(v) > 0) then
                        gFunc.Equip('Neck', 'Auditory Torque');
                        break;
                    end
                end
            end
        end
    end

    if (action.Skill == 'Blue Magic') then
        gFunc.Equip('Body', 'Magus Jubbah +1');
        if (action.Name == 'Magic Fruit' or action.Name == 'Healing Breeze'
            or action.Name == 'Wild Carrot') then
            gFunc.Equip('Neck', 'Fylgja Torque +1');
            gFunc.Equip('Back', 'Altruistic Cape');
        end
    elseif (action.Skill == 'Dark Magic') then
        need_macc = true;
        gFunc.Equip('Hands', 'Blood Fng. Gnt.');
        gFunc.Equip('Back', 'Merciful Cape');
    elseif (action.Skill == 'Healing Magic') then
        gFunc.Equip('Neck', 'Fylgja Torque +1');
        gFunc.Equip('Back', 'Dew Silk Cape +1');
    end

    if (need_macc) then
        gFunc.Equip('Head', 'Blood Mask');
        gFunc.Equip('Legs', 'Mirage Shalwar');
        gFunc.Equip('Waist', 'Salire Belt');
        gFunc.Equip('Ear1', 'Helenus\'s Earring');
        gFunc.Equip('Ear2', 'Cass. Earring');
        gFunc.Equip('Back', 'Mirage Mantle');
    end

    if (player.Status ~= 'Engaged') then
        common.SaveMP(sets['Idle_' .. settings.IdleVariant], maxmp_bonus['Idle_' .. settings.IdleVariant], settings.SaveMP);
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
    ['Requiescat']      = 'Requiescat',
    ['Expiacion']       = 'Expiacion',
    ['Vorpal Blade']    = 'MHWS',
    ['Flat Blade']      = 'MHWS',
};

local gorget_map = {
    ['Requiescat']      = 'Soil Gorget',
    ['Expiacion']       = 'Soil Gorget',
    ['Savage Blade']    = 'Soil Gorget',
    ['Vorpal Blade']    = 'Soil Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();

    gFunc.EquipSet('WS');

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end
end

return profile;
