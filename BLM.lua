local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant       = 'Default',
    TPThreshold     = 100,
    SaveMP          = false,
    MAccMode        = false,
    THMode          = true,
    ConvertHP       = true,
};

local maxmp_bonus = {
    ['Idle_Default'] = {
        Head    = 30,
        Body    = 50,
        Hands   = 17,
        Legs    = 13,
        Feet    = 12,
        Waist   = 48,
        Ears    = 30, -- Both Ear1 and Ear2
        Back    = 25,
    },
    ['Idle_MDT'] = {
        Head    = 30,
        Body    = 50,
        Hands   = 17,
        Legs    = 13,
        Feet    = 12,
        Waist   = 15,
    },
    ['Idle_Alchemy'] = {
        Head    = 30,
        Hands   = 17,
        Legs    = 13,
        Feet    = 12,
        Waist   = 15,
        Back    = 25,
    },
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Wzd. Petasos +1',
        Body    = 'Dalmatica',
        Hands   = 'Wzd. Gloves +1',
        Legs    = 'Sorcerer\'s Tonban',
        Feet    = 'Herald\'s Gaiters',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Merciful Cape',
    },
    ['Idle_MDT'] = {
        Head    = 'Wzd. Petasos +1',
        Body    = 'Dalmatica',
        Hands   = 'Wzd. Gloves +1',
        Legs    = 'Sorcerer\'s Tonban',
        Feet    = 'Herald\'s Gaiters',
        Neck    = 'Bloodbead Gorget',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Cheviot Cape',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Wzd. Petasos +1',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Wzd. Gloves +1',
        Legs    = 'Sorcerer\'s Tonban',
        Feet    = 'Herald\'s Gaiters',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Merciful Cape',
    },
    ['TP_Default'] = {
        Head    = 'Wzd. Petasos +1',
        Body    = 'Dalmatica',
        Hands   = 'Wzd. Gloves +1',
        Legs    = 'Sorcerer\'s Tonban',
        Feet    = 'Herald\'s Gaiters',
        Neck    = 'Beak Necklace +1',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Astral Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Vivian Ring',
        Back    = 'Merciful Cape',
    },
    ['TP_Accuracy'] = {
        Head    = 'Walahra Turban',
        Body    = 'Goliard Saio',
        Hands   = 'Hydra Gloves',
        Legs    = 'Hydra Brais',
        Feet    = 'Hydra Gaiters',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Pixie Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_MDT'] = {
        Head    = 'Wzd. Petasos +1',
        Body    = 'Dalmatica',
        Hands   = 'Wzd. Gloves +1',
        Legs    = 'Sorcerer\'s Tonban',
        Feet    = 'Herald\'s Gaiters',
        Neck    = 'Caract Choker',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Cheviot Cape',
    },
    ['Resting'] = {
        Head    = 'Wzd. Petasos +1',
        Body    = 'Sorcerer\'s Coat',
        Hands   = 'Hydra Gloves',
        Legs    = 'Hydra Brais',
        Feet    = 'Hydra Gaiters',
        Neck    = 'Beak Necklace +1',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Relaxing Earring',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Star Ring',
        Ring2   = 'Vivian Ring',
        Back    = 'Merciful Cape',
    },
    ['WS'] = {
        Head    = 'Optical Hat',
        Body    = 'Hydra Doublet',
        Hands   = 'Errant Cuffs',
        Legs    = 'Jet Seraweels',
        Feet    = 'Rostrum Pumps',
        Neck    = 'Peacock Amulet',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Morion Earring +1',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Rainbow Mantle',
    },
    ['Haste'] = {
        Head    = 'Walahra Turban',
        Body    = 'Goliard Saio',
        Feet    = 'Rostrum Pumps',
        Waist   = 'Speed Belt',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
        Back    = 'Veela Cape',
    },
    ['Precast'] = {
        Body    = 'Dalmatica',
        Feet    = 'Rostrum Pumps',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
        Back    = 'Veela Cape',
    },
    ['ConvertHP'] = {
        Head    = 'Zenith Crown +1',
        Body    = 'Dalmatica',
        Hands   = 'Zenith Mitts',
        Legs    = 'Zenith Slacks',
        Feet    = 'Zenith Pumps +1',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Astral Earring',
        Ring1   = 'Vilma\'s Ring',
        Ring2   = 'Vivian Ring',
        Back    = 'Aslan Cape',
    },
    ['ConvertHP_MAcc'] = {
        Head    = 'Zenith Crown +1',
        Body    = 'Dalmatica',
        Hands   = 'Zenith Mitts',
        Feet    = 'Zenith Pumps +1',
        Neck    = 'Morgana\'s Choker',
        Waist   = 'Hierarch Belt',
        Ear1    = 'Astral Earring',
        Ring1   = 'Vilma\'s Ring',
        Ring2   = 'Vivian Ring',
    },
    ['INT'] = {
        Head    = 'Zenith Crown +1',
        Body    = 'Errant Hpl.',
        Hands   = 'Errant Cuffs',
        Legs    = 'Errant Slops',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Lmg. Medallion +1',
        Waist   = 'Sorcerer\'s Belt',
        Ear1    = 'Abyssal Earring',
        Ear2    = 'Morion Earring +1',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Gleeman\'s Cape',
    },
    ['MND'] = {
        Head    = 'Errant Hat',
        Body    = 'Errant Hpl.',
        Hands   = 'Bricta\'s Cuffs',
        Legs    = 'Errant Slops',
        Feet    = 'Mahatma Pigaches',
        Neck    = 'Justice Badge',
        Waist   = 'Salire Belt',
        Ear1    = 'Celestial Earring',
        Ear2    = 'Harvest Earring',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Dew Silk Cape +1',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 8;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /tpt /lac fwd /tpt');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /smp /lac fwd /smp');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /chp /lac fwd /chp');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /macc /lac fwd /macc');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /heal <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /ma "Warp" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^b /ma "Stun" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^n /ma "Stun" <stnpc>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^c');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^b');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^n');
end

local th_timer = 0;
local target_index = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        elseif (args[2] == 'mdt') then
            if (settings.TPVariant ~= 'MDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'MDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'mdt') then
            settings.IdleVariant = 'MDT';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/tpt') then
        local number = tonumber(args[2]);

        if (number == nil or number < 0 or number > 3000) then
            gFunc.Error('Invalid argument: ' .. tostring(args[2]));
            return;
        else
            settings.TPThreshold = number;
        end
        gFunc.Message('TP threshold: ' .. args[2]);
    elseif (args[1] == '/smp') then
        settings.SaveMP = not settings.SaveMP;
        settings.ConvertHP = not settings.ConvertHP;
        gFunc.Message('Save MP: ' .. tostring(settings.SaveMP));
        gFunc.Message('Convert HP: ' .. tostring(settings.ConvertHP));
    elseif (args[1] == '/chp') then
        settings.ConvertHP = not settings.ConvertHP;
        settings.SaveMP = not settings.SaveMP;
        gFunc.Message('Convert HP: ' .. tostring(settings.ConvertHP));
        gFunc.Message('Save MP: ' .. tostring(settings.SaveMP));
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/macc') then
        settings.MAccMode = not settings.MAccMode;
        gFunc.Message('Magic Accuracy Mode: ' .. tostring(settings.MAccMode));
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    end
end

local sub;
local main;
local restore_weapons = false;

local subjob;
local counter = 0;
local macroset = 1;

local function RestoreWeapons()
    if (restore_weapons) then
        gFunc.Equip('Main', main);
        gFunc.Equip('Sub', sub);
        restore_weapons = false;
    end
end

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 5');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'SCH') and 4 or 3;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Feet', 'Herald\'s Gaiters');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        RestoreWeapons();
    elseif (player.Status == 'Resting') then
        gFunc.EquipSet('Resting');

        if (player.TP <= settings.TPThreshold and restore_weapons == false) then
            main = equipment.Main and equipment.Main.Name or nil;
            sub = equipment.Sub and equipment.Sub.Name or nil;
            restore_weapons = true;

            gFunc.Equip('Main', 'Chatoyant Staff');
            gFunc.Equip('Sub', 'Dark Grip');
        end
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
        RestoreWeapons();
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    local action = gData.GetAction();

    gFunc.EquipSet('Precast');

    if (string.match(action.Name, 'Cur[ae]')) then
        gFunc.Equip('Feet', 'Zenith Pumps +1');
    end

    if (settings.ConvertHP and action.Skill == 'Elemental Magic') then
        gFunc.SetMidDelay(1);
        if (settings.MAccMode) then
            gFunc.InterimEquipSet('ConvertHP_MAcc');
        else
            gFunc.InterimEquipSet('ConvertHP');
        end
    end
end

profile.HandleMidcast = function()
    local action = gData.GetAction();
    local player = gData.GetPlayer();
    local equipment = gData.GetEquipment();
    local environment = gData.GetEnvironment();

    if (action.Element ~= nil and action.Element ~= 'Unknown'
        and string.find(action.Type, 'Magic', 1, true) ~= nil
        and player.TP <= settings.TPThreshold and restore_weapons == false) then
        main = equipment.Main and equipment.Main.Name or nil;
        sub = nil;
        restore_weapons = true;

        gFunc.Equip('Main', common.staff_table[action.Element]);
        if (action.Name == 'Stoneskin') then
            gFunc.Equip('Main', 'Kirin\'s Pole');
        end
        gFunc.Equip('Sub', action.Element .. ' Grip');
    end

    if (action.Type == 'Black Magic') then
        gFunc.EquipSet('INT');
    elseif (action.Type == 'White Magic') then
        gFunc.EquipSet('MND');
    else
        gFunc.EquipSet('Haste');
    end

    if (action.Skill == 'Healing Magic') then
        gFunc.Equip('Body', 'Goliard Saio');
        gFunc.Equip('Feet', 'Shrewd Pumps');
        gFunc.Equip('Back', 'Dew Silk Cape +1');
        gFunc.Equip('Neck', 'Fylgja Torque +1');
    elseif (action.Skill == 'Enhancing Magic') then
        gFunc.Equip('Body', 'Goliard Saio');
        gFunc.Equip('Feet', 'Shrewd Pumps');
        gFunc.Equip('Neck', 'Enhancing Torque');
        gFunc.Equip('Back', 'Merciful Cape');
    elseif (action.Skill == 'Enfeebling Magic') then
        gFunc.Equip('Head', 'Sorcerer\'s Petas.');
        gFunc.Equip('Body', 'Wizard\'s Coat');
        gFunc.Equip('Hands', 'Bricta\'s Cuffs');
        gFunc.Equip('Legs', 'Shadow Trews');
        gFunc.Equip('Waist', 'Salire Belt');
        gFunc.Equip('Ear1', 'Helenus\'s Earring');
        gFunc.Equip('Ear2', 'Cass. Earring');
        gFunc.Equip('Back', 'Altruistic Cape');
    elseif (action.Skill == 'Elemental Magic') then
        gFunc.Equip('Head', 'Zenith Crown +1');

        if (settings.MAccMode) then
            gFunc.Equip('Body', 'Shadow Coat');
            gFunc.Equip('Hands', 'Wzd. Gloves +1');
        else
            gFunc.Equip('Body', 'Igqira Weskit');
            gFunc.Equip('Hands', 'Zenith Mitts');
        end

        if (action.Element == environment.DayElement) then
            gFunc.Equip('Legs', 'Sorcerer\'s Tonban');
        else
            gFunc.Equip('Legs', 'Shadow Trews');
        end

        gFunc.Equip('Feet', 'Yigit Crackows');
        gFunc.Equip('Waist', 'Salire Belt');

        if ((player.HPP <= 75 and player.TP < 1000) or settings.ConvertHP) then
            gFunc.Equip('Ring2', 'Sorcerer\'s Ring');
        end

        gFunc.Equip('Ear1', 'Helenus\'s Earring');
        gFunc.Equip('Ear2', 'Cass. Earring');
    elseif (action.Skill == 'Dark Magic') then
        gFunc.Equip('Head', 'Zenith Crown +1');
        gFunc.Equip('Hands', 'Sorcerer\'s Gloves');
        gFunc.Equip('Legs', 'Wizard\'s Tonban');
        gFunc.Equip('Waist', 'Salire Belt');
        gFunc.Equip('Ear1', 'Helenus\'s Earring');
        gFunc.Equip('Ear2', 'Cass. Earring');
        gFunc.Equip('Back', 'Merciful Cape');
    end

    common.EvaluateObi(action.Element);
    if (player.Status ~= 'Engaged') then
        common.SaveMP(sets['Idle_' .. settings.IdleVariant], maxmp_bonus['Idle_' .. settings.IdleVariant], settings.SaveMP);
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
};

local gorget_map = {
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();

    gFunc.EquipSet('WS');

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end
end

return profile;
