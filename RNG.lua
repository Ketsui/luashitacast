local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant      = 'Default',
    WSetVariant     = 'Default',
    THMode          = true,
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Blood Mask',
        Body    = 'Blood Scale Mail',
        Hands   = 'Blood Fng. Gnt.',
        Legs    = 'Blood Cuisses',
        Feet    = 'Trotter Boots',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Blood Mask',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Blood Fng. Gnt.',
        Legs    = 'Blood Cuisses',
        Feet    = 'Trotter Boots',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_Default'] = {
        Head    = 'Walahra Turban',
        Body    = 'Pln. Khazagand',
        Hands   = 'Pln. Dastanas',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Suppanomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Walahra Turban',
        Body    = 'Pln. Khazagand',
        Hands   = 'Pln. Dastanas',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Suppanomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_MDT'] = {
        Head    = 'Walahra Turban',
        Body    = 'Pln. Khazagand',
        Hands   = 'Pln. Dastanas',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Caract Choker',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['WS_Default'] = {
        Head    = 'Htr. Beret +1',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Seiryu\'s Kote',
        Legs    = 'Htr. Braccae +1',
        Feet    = 'Scout\'s Socks',
        Neck    = 'Hope Torque',
        Waist   = 'Scout\'s Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Emerald Ring',
        Ring2   = 'Emerald Ring',
        Back    = 'Amemet Mantle +1',
    },
    ['WS_Accuracy'] = {
        Head    = 'Htr. Beret +1',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Seiryu\'s Kote',
        Legs    = 'Htr. Braccae +1',
        Feet    = 'Scout\'s Socks',
        Neck    = 'Hope Torque',
        Waist   = 'Scout\'s Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Emerald Ring',
        Ring2   = 'Emerald Ring',
        Back    = 'Amemet Mantle +1',
    },
    ['Preshot'] = {
        Head    = 'Htr. Beret +1',
        Body    = 'Scout\'s Jerkin',
        Hands   = 'Blood Fng. Gnt.',
    },
    ['Midshot'] = {
        Head    = 'Zha\'Go\'s Barbut',
        Body    = 'Htr. Jerkin +1',
        Hands   = 'Seiryu\'s Kote',
        Legs    = 'Htr. Braccae +1',
        Feet    = 'Blood Greaves',
        Neck    = 'Hope Torque',
        Waist   = 'Scout\'s Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Merman\'s Ring',
        Ring2   = 'Rajas Ring',
        Back    = 'Amemet Mantle +1',
    },
    ['Haste'] = {
        Head    = 'Acubens Helm',
        Body    = 'Blood Scale Mail',
        Hands   = 'Dusk Gloves',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Tiercel Necklace',
        Waist   = 'Speed Belt',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Legs    = 'Blood Cuisses',
        Feet    = 'Suzaku\'s Sune-Ate',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 16;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^z /ja "Unlimited Shot" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /ja "Stealth Shot" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /ja "Scavenge" <me>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^s /ra <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^h /ws "Apex Arrow" <stnpc>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^z');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^c');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^s');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^h');
end

local th_timer = 0;
local target_index = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        elseif (args[2] == 'mdt') then
            if (settings.TPVariant ~= 'MDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'MDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    end
end

local subjob;
local counter = 0;
local macroset = 0;

local old_ammo = nil;

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 23');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'WAR') and 2 or
                    (player.SubJob == 'DNC') and 3 or
                    (player.SubJob == 'NIN') and 4 or
                    (player.SubJob == 'SAM') and 5 or
                    (player.SubJob == 'DRG') and 6 or
                    (player.SubJob == 'DRK') and 7 or
                    (player.SubJob == 'THF') and 8 or 2;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (old_ammo ~= nil) then
        gFunc.Equip('Ammo', old_ammo);
        old_ammo = nil;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Legs', 'Blood Cuisses');
            gFunc.Equip('Feet', 'Trotter Boots');
        end

        if (player.SubJob == 'DRG') then
            gFunc.Equip('Ear2', 'Wyvern Earring');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        common.RemoveSleep();
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (action.Name == 'Shadowbind') then
        gFunc.Equip('Hands', 'Hunter\'s Bracer');
    elseif (action.Name == 'Sharpshot') then
        gFunc.Equip('Legs', 'Htr. Braccae +1');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    gFunc.EquipSet('Precast');
end

profile.HandleMidcast = function()
    local player = gData.GetPlayer();

    gFunc.EquipSet('Haste');

    if (player.SubJob == 'DRG') then
        gFunc.Equip('Ear2', 'Wyvern Earring');
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    gFunc.EquipSet('Midshot');

    if (th_timer <= 20 and settings.THMode == true) then
        th_timer = 21;
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
};

local gorget_map = {
    ['Apex Arrow'] = 'Thunder Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();
    local equipment = gData.GetEquipment();
    local environment = gData.GetEnvironment();

    if (equipment ~= nil) then
        old_ammo = equipment.Ammo and equipment.Ammo.Name or nil;
        gFunc.Equip('Ammo', 'Kabura Arrow');
    end

    gFunc.EquipSet('WS_' .. settings.WSetVariant);

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end

    common.RemoveSleep();
end

return profile;
