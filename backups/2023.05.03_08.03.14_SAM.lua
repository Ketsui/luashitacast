local profile = {};

local Settings = {
    TPVariant = 'Default'
};

local sets = {
    ['Idle'] = {
        Head = 'Emperor Hairpin',
        Neck = 'Focus Collar',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Body = 'Scorpion Harness',
        Hands = 'Ryl.Kgt. Mufflers',
        Ring1 = 'Peridot Ring',
        Ring2 = 'Warp Ring',
        Back = 'Behemoth Mantle',
        Legs = 'Ryl.Sqr. Breeches',
        Feet = 'Myochin Sune-Ate',
    },
    ['TP_Default'] = {
        Head = 'Emperor Hairpin',
        Neck = 'Focus Collar',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Body = 'Scorpion Harness',
        Hands = 'Ryl.Kgt. Mufflers',
        Ring1 = 'Sniper\'s Ring',
        Ring2 = 'Sniper\'s Ring',
        Back = 'Behemoth Mantle',
        Legs = 'Ryl.Sqr. Breeches',
        Feet = 'Myochin Sune-Ate',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
end

profile.OnUnload = function()
end

profile.HandleCommand = function(args)
end

profile.HandleDefault = function()
    local player = gData.GetPlayer();

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. Settings.TPVariant);
    else
        gFunc.EquipSet(sets.Idle);
    end
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (action.Name == 'Meditate') or (action.Name == 'Warding Circle') then
        gFunc.Equip('Head', 'Myochin Kabuto');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
end

profile.HandleMidcast = function()
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
end

profile.HandleWeaponskill = function()
end

return profile;
