local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    SetVariant      = 'Default',
    THMode          = true,
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Genbu\'s Kabuto',
        Body    = 'Nocturnus Mail',
        Hands   = 'Myn. Kote +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Genbu\'s Kabuto',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Myn. Kote +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Suzaku\'s Sune-Ate',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_Default'] = {
        Head    = 'Walahra Turban',
        Body    = 'Nocturnus Mail',
        Hands   = 'Myn. Kote +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Myn. Sune-Ate +1',
        Neck    = 'Peacock Amulet',
        Waist   = 'Swift Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Ace\'s Helm',
        Body    = 'Nocturnus Mail',
        Hands   = 'Myn. Kote +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Myn. Sune-Ate +1',
        Neck    = 'Peacock Amulet',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['WS_Default'] = {
        Head    = 'Shr.Znr.Kabuto',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Alkyoneus\'s Brc.',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Rutter Sabatons',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['WS_Accuracy'] = {
        Head    = 'Shr.Znr.Kabuto',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Myn. Kote +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Rutter Sabatons',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['Requiescat'] = {
        Head    = 'Shr.Znr.Kabuto',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Myn. Kote +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Myn. Sune-Ate +1',
        Neck    = 'Soil Gorget',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Suppanomimi',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Aesir Mantle',
    },
    ['Midshot'] = {
        Head    = 'Optical Hat',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Seiryu\'s Kote',
        Legs    = 'Dusk Trousers',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Peacock Amulet',
        Waist   = 'Ryl.Kgt. Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Merman\'s Ring',
        Ring2   = 'Coral Ring',
        Back    = 'Smilod. Mantle +1',
    },
    ['Haste'] = {
        Head    = 'Walahra Turban',
        Hands   = 'Myn. Kote +1',
        Legs    = 'Byakko\'s Haidate',
        Feet    = 'Myn. Sune-Ate +1',
        Waist   = 'Swift Belt',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 9;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /ws "Tachi: Rana" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /ws "Penta Thrust" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^b /ws "Tachi: Shoha" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Tachi: Yukikaze" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Tachi: Gekko" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Tachi: Kasha" <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^c');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^b');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
end

local th_timer = 0;
local target_index = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.SetVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.SetVariant = 'Default';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Set variant: ' .. settings.SetVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    end
end

local subjob;
local counter = 0;
local macroset = 0;
local sword_mode = false;

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 17');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'WAR') and 2 or
                    (player.SubJob == 'DNC') and 3 or
                    (player.SubJob == 'NIN') and 4 or
                    (player.SubJob == 'RNG') and 5 or
                    (player.SubJob == 'DRG') and 6 or
                    (player.SubJob == 'DRK') and 7 or
                    (player.SubJob == 'THF') and 8 or 2;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (equipment.Main ~= nil and equipment.Main.Name == 'Concordia' and sword_mode == false) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Requiescat" <t>');
        sword_mode = true;
    elseif (equipment.Main ~= nil and equipment.Main.Name ~= 'Concordia'
            and sword_mode == true) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Tachi: Gekko" <t>');
        sword_mode = false;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.SetVariant);

        if (equipment.Main ~= nil and equipment.Main.Name == 'Concordia') then
            gFunc.Equip('Ear2', 'Fowling Earring');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        common.RemoveSleep();
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (action.Name == 'Meditate') or (action.Name == 'Warding Circle') then
        gFunc.Equip('Head', 'Myochin Kabuto');
        gFunc.Equip('Hands', 'Saotome Kote');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    gFunc.EquipSet('Precast');
end

profile.HandleMidcast = function()
    gFunc.EquipSet('Haste');
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    gFunc.EquipSet('Midshot');

    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
    ['Requiescat'] = 'Requiescat',
};

local gorget_map = {
    ['Requiescat']      = 'Soil Gorget',
    ['Savage Blade']    = 'Soil Gorget',
    ['Vorpal Blade']    = 'Soil Gorget',
    ['Tachi: Shoha']    = 'Shadow Gorget',
    ['Tachi: Kasha']    = 'Shadow Gorget',
    ['Tachi: Rana']     = 'Shadow Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();
    local environment = gData.GetEnvironment();

    gFunc.EquipSet('WS_' .. settings.SetVariant);

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end

    if ((environment.Time >= 18 or environment.Time < 6)
        and string.match(action.Name, 'Tachi')) then
        gFunc.Equip('Ear1', 'Vampire Earring');
        gFunc.Equip('Ear2', 'Vampire Earring');
    end

    common.RemoveSleep();
end

return profile;
