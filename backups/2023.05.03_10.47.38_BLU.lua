local profile = {};
local sets = {
    ['STR_DEX'] = {
        Head = 'Emperor Hairpin',
        Body = 'Magus Jubbah',
        Hands = 'Light Gauntlets',
        Legs = 'Phl. Trousers',
        Feet = 'Magus Charuqs',
        Neck = 'Focus Collar',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Sniper\'s Ring',
        Ring2 = 'Sniper\'s Ring',
        Back = 'Behemoth Mantle',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
end

profile.OnUnload = function()
end

profile.HandleCommand = function(args)
end

profile.HandleDefault = function()
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
end

profile.HandleMidcast = function()
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
end

profile.HandleWeaponskill = function()
end

return profile;
