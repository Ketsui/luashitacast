local profile = {};

local settings = {
    TPVariant = 'Default'
};

local sets = {
    ['Idle'] = {
        Head = 'Walahra Turban',
        Body = 'Scorpion Harness',
        Hands = 'Magus Bazubands',
        Legs = 'Magus Shalwar',
        Feet = 'Magus Charuqs',
        Neck = 'Orochi Nodowa',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Ruby Ring',
        Ring2 = 'Warp Ring',
        Back = 'Cerberus Mantle',
    },
    ['TP_Default'] = {
        Head = 'Walahra Turban',
        Body = 'Scorpion Harness',
        Hands = 'Dusk Gloves',
        Legs = 'Phl. Trousers',
        Feet = 'Leaping Boots',
        Neck = 'Spike Necklace',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Sniper\'s Ring',
        Ring2 = 'Sniper\'s Ring',
        Back = 'Cerberus Mantle',
    },
    ['WS'] = {
        Head = 'Voyager Sallet',
        Body = 'Assault Jerkin',
        Hands = 'Tarasque Mitts',
        Legs = 'Phl. Trousers',
        Feet = 'Leaping Boots',
        Neck = 'Orochi Nodowa',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Ruby Ring',
        Ring2 = 'Ruby Ring',
        Back = 'Cerberus Mantle',
    },
    ['MHWS'] = {
        Head = 'Voyager Sallet',
        Body = 'Scorpion Harness',
        Hands = 'Combat Mittens',
        Legs = 'Phl. Trousers',
        Feet = 'Leaping Boots',
        Neck = 'Spike Necklace',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Sniper\'s Ring',
        Ring2 = 'Sniper\'s Ring',
        Back = 'Cerberus Mantle',
    },
    ['Haste'] = {
        Head = 'Walahra Turban',
        Body = 'Scorpion Harness',
        Hands = 'Dusk Gloves',
        Legs = 'Magus Shalwar',
        Feet = 'Magus Charuqs',
        Neck = 'Orochi Nodowa',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Ruby Ring',
        Ring2 = 'Ruby Ring',
        Back = 'Cerberus Mantle',
    },
    ['STR_DEX'] = {
        Head = 'Voyager Sallet',
        Body = 'Magus Jubbah',
        Hands = 'Light Gauntlets',
        Legs = 'Phl. Trousers',
        Feet = 'Leaping Boots',
        Neck = 'Spike Necklace',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Ruby Ring',
        Ring2 = 'Ruby Ring',
        Back = 'Smilod. Mantle +1',
    },
    ['DEX'] = {
        Head = 'Voyager Sallet',
        Body = 'Magus Jubbah',
        Hands = 'Combat Mittens',
        Legs = 'Magus Shalwar',
        Feet = 'Leaping Boots',
        Neck = 'Spike Necklace',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Sniper\'s Ring',
        Ring2 = 'Sniper\'s Ring',
        Back = 'Smilod. Mantle +1',
    },
    ['STR_VIT'] = {
        Head = 'Voyager Sallet',
        Body = 'Magus Jubbah',
        Hands = 'Light Gauntlets',
        Legs = 'Magus Shalwar',
        Feet = 'Leaping Boots',
        Neck = 'Spike Necklace',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Heims Earring',
        Ring1 = 'Ruby Ring',
        Ring2 = 'Ruby Ring',
        Back = 'Smilod. Mantle +1',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local player = gData.GetPlayer();
    local macrobook = 4;
    local macroset = (player.SubJob == 'SAM') and 2 or
                        (player.SubJob == 'SCH') and 3 or
                        (player.SubJob == 'THF') and 5 or
                        (player.SubJob == 'WAR') and 6 or
                        (player.SubJob == 'DNC') and 7 or
                        (player.SubJob == 'NIN') and 8 or 1;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 1');
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Savage Blade" <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
end

profile.HandleCommand = function(args)
end

local subjob;

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local equipment = gData.GetEquipment();

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);
    else
        gFunc.EquipSet(sets.Idle);
    end

    if (equipment.Hands ~= nil and equipment.Hands.Name == 'Magus Bazubands') then
        gFunc.Equip('Hands', equipment.Hands.Name);
    end
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
end

profile.HandleMidcast = function()
    local action = gData.GetAction();

    if (action.Name == 'Frenetic Rip') then
        gFunc.EquipSet(sets.STR_DEX);
    elseif (action.Name == 'Hysteric Barrage') then
        gFunc.EquipSet(sets.DEX);
    elseif (action.Name == 'Cannonball') then
        gFunc.EquipSet(sets.STR_VIT);
    else
        gFunc.EquipSet(sets.Haste);
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
end

profile.HandleWeaponskill = function()
    local action = gData.GetAction();

    if (action.Name == 'Vorpal Blade') then
        gFunc.EquipSet(sets.MHWS);
    else
        gFunc.EquipSet(sets.WS);
    end
end

return profile;
