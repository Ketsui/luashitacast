local profile = {};

local settings = {
    TPVariant = 'Default',
    TPThreshold = 100,
};

local sets = {
    ['Idle'] = {
        Head = 'Walahra Turban',
        Body = 'Mirage Jubbah',
        Hands = 'Mirage Bazubands',
        Legs = 'Mirage Shalwar',
        Feet = 'Magus Charuqs',
        Neck = 'Orochi Nodowa',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Astral Earring',
        Ear2 = 'Merman\'s Earring',
        Ring1 = 'Warp Ring',
        Ring2 = 'Ruby Ring',
        Back = 'Mirage Mantle',
    },
    ['TP_Default'] = {
        Head = 'Walahra Turban',
        Body = 'Mirage Jubbah',
        Hands = 'Dusk Gloves',
        Legs = 'Mirage Shalwar',
        Feet = 'Dusk Ledelsens',
        Neck = 'Spike Necklace',
        Waist = 'Headlong Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Kusha\'s Ring',
        Ring2 = 'Lava\'s Ring',
        Back = 'Mirage Mantle',
    },
    ['TP_Accuracy'] = {
        Head = 'Optical Hat',
        Body = 'Mirage Jubbah',
        Hands = 'Combat Mittens',
        Legs = 'Mirage Shalwar',
        Feet = 'Leaping Boots',
        Neck = 'Spike Necklace',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Kusha\'s Ring',
        Ring2 = 'Lava\'s Ring',
        Back = 'Mirage Mantle',
    },
    ['WS'] = {
        Head = 'Voyager Sallet',
        Body = 'Assault Jerkin',
        Hands = 'Tarasque Mitts',
        Legs = 'Dusk Trousers',
        Feet = 'Leaping Boots',
        Neck = 'Orochi Nodowa',
        Waist = 'Fierce Belt',
        Ear1 = 'Merman\'s Earring',
        Ear2 = 'Merman\'s Earring',
        Ring1 = 'Kusha\'s Ring',
        Ring2 = 'Lava\'s Ring',
        Back = 'Cerberus Mantle',
    },
    ['MHWS'] = {
        Head = 'Optical Hat',
        Body = 'Assault Jerkin',
        Hands = 'Tarasque Mitts',
        Legs = 'Dusk Trousers',
        Feet = 'Leaping Boots',
        Neck = 'Spike Necklace',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Merman\'s Earring',
        Ear2 = 'Merman\'s Earring',
        Ring1 = 'Kusha\'s Ring',
        Ring2 = 'Lava\'s Ring',
        Back = 'Mirage Mantle',
    },
    ['Requiescat'] = {
        Head = 'Optical Hat',
        Body = 'Assault Jerkin',
        Hands = 'Mirage Bazubands',
        Legs = 'Dusk Trousers',
        Feet = 'Mahatma Pigaches',
        Neck = 'Soil Gorget',
        Waist = 'Virtuoso Belt',
        Ear1 = 'Merman\'s Earring',
        Ear2 = 'Merman\'s Earring',
        Ring1 = 'Sapphire Ring',
        Ring2 = 'Sapphire Ring',
        Back = 'Mirage Mantle',
    },
    ['Haste'] = {
        Head = 'Walahra Turban',
        Body = 'Magus Jubbah',
        Hands = 'Dusk Gloves',
        Legs = 'Magus Shalwar',
        Feet = 'Dusk Ledelsens',
        Waist = 'Headlong Belt',
    },
    ['INT'] = {
        Head = 'Errant Hat',
        Body = 'Errant Hpl.',
        Hands = 'Errant Cuffs',
        Legs = 'Errant Slops',
        Feet = 'Wood F Ledelsens',
        Neck = 'Philomath Stole',
        Waist = 'Ryl.Kgt. Belt',
        Ear1 = 'Heims Earring',
        Ear2 = 'Moldavite Earring',
        Ring1 = 'Diamond Ring',
        Ring2 = 'Diamond Ring',
        Back = 'Rainbow Cape',
    },
    ['MND'] = {
        Head = 'Errant Hat',
        Body = 'Errant Hpl.',
        Hands = 'Mirage Bazubands',
        Legs = 'Errant Slops',
        Feet = 'Mahatma Pigaches',
        Neck = 'Justice Badge',
        Waist = 'Ryl.Kgt. Belt',
        Ear2 = 'Harvest Earring',
        Ring1 = 'Sapphire Ring',
        Ring2 = 'Sapphire Ring',
        Back = 'Rainbow Cape',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 4;
    local player = gData.GetPlayer();

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /tpt /lac fwd /tpt');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Savage Blade" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Requiescat" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Expiacion" <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
end

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        else
            gFunc.Error('Unknown set: %s' .. args[2]);
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/tpt') then
        local number = tonumber(args[2]);

        if (number == nil or number < 0 or number > 3000) then
            gFunc.Error('Invalid argument: ' .. args[2]);
            return;
        else
            settings.TPThreshold = number;
        end
        gFunc.Message('TP threshold: ' .. args[2]);
    end
end

local sub;
local main;
local restore_weapons = false;

local subjob;
local counter = 0;
local macroset = 1;

local th_timer = 0;
local target_index = nil;

local function RestoreWeapons()
    if (restore_weapons) then
        gFunc.Equip('Main', main);
        gFunc.Equip('Sub', sub);
        restore_weapons = false;
    end
end

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 4');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'WHM') and 3 or
                    (player.SubJob == 'SCH') and 3 or
                    (player.SubJob == 'BLM') and 3 or 7;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Hands', 'Combat Mittens');
            gFunc.Equip('Feet', 'Leaping Boots');
        end

        if (th_timer <= 20) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        RestoreWeapons();
    else
        gFunc.EquipSet('Idle');
        RestoreWeapons();
    end

    if (equipment.Ring2 ~= nil and equipment.Ring2.Name == 'Warp Ring') then
        gFunc.Equip('Ring2', equipment.Ring2.Name);
    end
    if (equipment.Ring2 ~= nil and equipment.Ring2.Name == 'Echad Ring') then
        gFunc.Equip('Ring2', equipment.Ring2.Name);
    end
    if (equipment.Head ~= nil and equipment.Head.Name == 'Sprout Beret') then
        gFunc.Equip('Head', equipment.Head.Name);
    end
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
end

local buff_spells = {
    'Refresh', 'Haste', 'Blink',
};

local staff_map = {
    ['Fire'] = 'Iridal Staff',
    ['Ice'] = 'Iridal Staff',
    ['Wind'] = 'Auster\'s Staff',
    ['Earth'] = 'Iridal Staff',
    ['Thunder'] = 'Iridal Staff',
    ['Water'] = 'Iridal Staff',
    ['Light'] = 'Iridal Staff',
    ['Dark'] = 'Iridal Staff',
    ['Non-Elemental'] = 'Iridal Staff',
};

profile.HandleMidcast = function()
    local need_staff = true;
    local action = gData.GetAction();
    local player = gData.GetPlayer();
    local equipment = gData.GetEquipment();

    for _, v in ipairs(buff_spells) do
        if (action.Name == v) then
            need_staff = false;
        end
    end

    if (need_staff and action.Element ~= nil and action.Element ~= 'Unknown' and
        (action.Element ~= 'Non-Elemental'
        or action.Name == 'Wild Carrot' or action.Name == 'Magic Fruit' or action.Name == 'Healing Breeze')
        and string.find(action.Type, 'Magic', 1, true) ~= nil
        and player.TP <= settings.TPThreshold and restore_weapons == false) then
        main = equipment.Main ~= nil and equipment.Main.Name or nil;
        sub = equipment.Sub ~= nil and equipment.Sub.Name or nil;
        restore_weapons = true;

        for k, v in pairs(staff_map) do
            if (action.Element == k) then
                gFunc.Equip('Main', v);
                break;
            end
        end
        gFunc.Equip('Sub', action.Element .. ' Grip');
    end

    if (action.Type == 'Black Magic') then
        gFunc.EquipSet('INT');
    elseif (action.Type == 'White Magic') then
        gFunc.EquipSet('MND');
    else
        gFunc.EquipSet('Haste');
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
    ['Requiescat'] = 'Requiescat',
    ['Vorpal Blade'] = 'MHWS',
    ['Flat Blade'] = 'MHWS',
};

local gorget_map = {
    ['Requiescat'] = 'Soil Gorget',
    ['Savage Blade'] = 'Soil Gorget',
    ['Vorpal Blade'] = 'Soil Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();

    gFunc.EquipSet('WS');

    for k, v in pairs(ws_map) do
        if (action.Name == k) then
            gFunc.EquipSet(v);
        end
    end

    for k, v in pairs(gorget_map) do
        if (action.Name == k) then
            gFunc.Equip('Neck', v);
        end
    end
end

return profile;
