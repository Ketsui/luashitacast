local profile = {};
local sets = {
    ['Idle'] = {
        Main = 'Namioyogi',
        Sub = 'Pole Grip',
        Range = 'Self Bow',
        Ammo = 'Wooden Arrow',
        Head = 'Emperor Hairpin',
        Neck = 'Focus Collar',
        Ear1 = 'Minuet Earring',
        Ear2 = 'Harvest Earring',
        Body = 'Scorpion Harness',
        Hands = 'Ryl.Kgt. Mufflers',
        Ring1 = 'Peridot Ring',
        Ring2 = 'Warp Ring',
        Back = 'Behemoth Mantle',
        Legs = 'Ryl.Sqr. Breeches',
        Feet = 'Alumine Sollerets',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    gSettings.AllowAddSet = true;
end

profile.OnUnload = function()
end

profile.HandleCommand = function(args)
end

profile.HandleDefault = function()
end

profile.HandleAbility = function()
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
end

profile.HandleMidcast = function()
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
end

profile.HandleWeaponskill = function()
end

return profile;