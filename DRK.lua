local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant      = 'Default',
    THMode          = true,
    AnimeMode       = true,
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Blood Mask',
        Body    = 'Shadow Brstplate',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Blood Cuisses',
        Feet    = 'Homam Gambieras',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Blood Mask',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Blood Cuisses',
        Feet    = 'Homam Gambieras',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_Default'] = {
        Head    = 'Chs. Burgeonet +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Chs. Sollerets +1',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Ace\'s Helm',
        Body    = 'Adaman Hauberk',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Homam Gambieras',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Abyss Cape',
    },
    ['TP_PDT'] = {
        Head    = 'Chs. Burgeonet +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Chs. Sollerets +1',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['TP_MDT'] = {
        Head    = 'Chs. Burgeonet +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Chs. Sollerets +1',
        Neck    = 'Ancient Torque',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['TP_BDT'] = {
        Head    = 'Chs. Burgeonet +1',
        Body    = 'Blood Scale Mail',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Chs. Sollerets +1',
        Neck    = 'Ancient Torque',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['WS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Shadow Cuishes',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Fierce Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['MHWS'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Bahram Cuisses',
        Feet    = 'Hct. Leggings',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['Requiescat'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Bahram Cuisses',
        Feet    = 'Chs. Sollerets +1',
        Neck    = 'Soil Gorget',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Suppanomimi',
        Ring1   = 'Aqua Ring',
        Ring2   = 'Star Ring',
        Back    = 'Aesir Mantle',
    },
    ['Resolution'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Bahram Cuisses',
        Feet    = 'Chs. Sollerets +1',
        Neck    = 'Soil Gorget',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Aesir Mantle',
    },
    ['Catastrophe'] = {
        Head    = 'Chs. Burgeonet +1',
        Body    = 'Zahak\'s Mail',
        Hands   = 'Hecatomb Mittens',
        Legs    = 'Hecatomb Subligar',
        Feet    = 'Hct. Leggings',
        Neck    = 'Soil Gorget',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Abyssal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Forager\'s Mantle',
    },
    ['Entropy'] = {
        Head    = 'Hecatomb Cap +1',
        Body    = 'Adaman Hauberk',
        Hands   = 'Abs. Gauntlets +1',
        Legs    = 'Shadow Cuishes',
        Feet    = 'Chs. Sollerets +1',
        Neck    = 'Soil Gorget',
        Waist   = 'Warwolf Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Abyssal Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Galdr Ring',
        Back    = 'Aesir Mantle',
    },
    ['Haste'] = {
        Head    = 'Chs. Burgeonet +1',
        Body    = 'Blood Scale Mail',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Homam Cosciales',
        Feet    = 'Chs. Sollerets +1',
        Waist   = 'Speed Belt',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Legs    = 'Homam Cosciales',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Midshot'] = {
        Head    = 'Optical Hat',
        Body    = 'Hecatomb Harness',
        Hands   = 'Crimson Fng. Gnt.',
        Legs    = 'Dusk Trousers',
        Feet    = 'Homam Gambieras',
        Neck    = 'Peacock Amulet',
        Waist   = 'Ryl.Kgt. Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Merman\'s Ring',
        Ring2   = 'Coral Ring',
        Back    = 'Amemet Mantle +1',
    },
    ['Accuracy'] = {
        Head    = 'Optical Hat',
        Body    = 'Nocturnus Mail',
        Hands   = 'Chs. Gauntlets +1',
        Legs    = 'Bahram Cuisses',
        Feet    = 'Homam Gambieras',
        Neck    = 'Chivalrous Chain',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Fowling Earring',
        Ring1   = 'Sniper\'s Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Abyss Cape',
    },
    ['Dark Magic'] = {
        Head    = 'Chs. Burgeonet +1',
        Body    = 'Demon\'s Harness',
        Hands   = 'Blood Fng. Gnt.',
        Legs    = 'Chs. Flanchard +1',
        Feet    = 'Wood F Ledelsens',
        Neck    = 'Lmg. Medallion +1',
        Waist   = 'Salire Belt',
        Ear1    = 'Helenus\'s Earring',
        Ear2    = 'Cass. Earring',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Abyss Cape',
    },
    ['Enfeebling Magic'] = {
        Head    = 'Blood Mask',
        Body    = 'Chaos Cuirass',
        Hands   = 'Abs. Gauntlets +1',
        Legs    = 'Chs. Flanchard +1',
        Feet    = 'Abyss Sollerets',
        Neck    = 'Lmg. Medallion +1',
        Waist   = 'Salire Belt',
        Ear1    = 'Helenus\'s Earring',
        Ear2    = 'Cass. Earring',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Abyss Cape',
    },
    ['Elemental Magic'] = {
        Head    = 'Demon Helm',
        Body    = 'Blood Scale Mail',
        Hands   = 'Abs. Gauntlets +1',
        Legs    = 'Chs. Flanchard +1',
        Feet    = 'Shrewd Pumps',
        Neck    = 'Lmg. Medallion +1',
        Waist   = 'Salire Belt',
        Ear1    = 'Helenus\'s Earring',
        Ear2    = 'Cass. Earring',
        Ring1   = 'Galdr Ring',
        Ring2   = 'Diamond Ring',
        Back    = 'Abyss Cape',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 2;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /anime /lac fwd /anime');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^w /ma "Absorb-TP" <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^z /ma "Drain II" <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^x /ma "Drain" <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^d /ws "Entropy" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^f /ws "Requiescat" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^g /ws "Resolution" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^c /ws "Catastrophe" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^b /ma "Stun" <t>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^n /ma "Stun" <stnpc>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^w');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^z');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^x');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^d');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^f');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^g');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^c');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^b');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^n');
end

local th_timer = 0;
local target_index = nil;

local prev_set = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        elseif (args[2] == 'pdt') then
            if (settings.TPVariant ~= 'PDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'PDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        elseif (args[2] == 'mdt') then
            if (settings.TPVariant ~= 'MDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'MDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        elseif (args[2] == 'bdt') then
            if (settings.TPVariant ~= 'BDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'BDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    elseif (args[1] == '/anime') then
        settings.AnimeMode = not settings.AnimeMode;
        gFunc.Message('Anime Mode: ' .. tostring(settings.AnimeMode));
    end
end

local engaged_lockstyle = 0;

local ChangeLockstyle = function()
    local player = gData.GetPlayer();

    if (player.Status == 'Engaged' and engaged_lockstyle == 0) then
        engaged_lockstyle = 1;
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 15');
    elseif (player.Status == 'Idle' and engaged_lockstyle == 1) then
        engaged_lockstyle = 0;
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 14');
    end
end

local sub;
local main;
local restore_weapons = false;

local function RestoreWeapons()
    if (restore_weapons) then
        gFunc.Equip('Main', main);
        gFunc.Equip('Sub', sub);
        restore_weapons = false;
    end
end

local subjob;
local counter = 0;
local macroset = 0;

profile.HandleDefault = function()
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 14');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'NIN') and 2 or
                    (player.SubJob == 'SAM') and 4 or
                    (player.SubJob == 'WAR') and 5 or
                    (player.SubJob == 'THF') and 6 or
                    (player.SubJob == 'DRG') and 7 or 2;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (player.IsMoving) then
            gFunc.Equip('Legs', 'Blood Cuisses');
        end

        if (equipment.Main ~= nil and equipment.Main.Name == 'Apocalypse') then
            gFunc.Equip('Ear2', 'Abyssal Earring');
        end

        if (player.SubJob == 'DRG') then
            gFunc.Equip('Ear2', 'Wyvern Earring');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        common.RemoveSleep();

        if (settings.AnimeMode) then
            ChangeLockstyle();
        end
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);

        if (settings.AnimeMode) then
            ChangeLockstyle();
        end
    end

    RestoreWeapons();
    common.CheckManualEquips();
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (action.Name == 'Weapon Bash') then
        gFunc.EquipSet('Accuracy');
    elseif (action.Name == 'Souleater') then
        gFunc.Equip('Head', 'Chs. Burgeonet +1');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    if (gData.GetBuffCount('Hasso') > 0 or gData.GetBuffCount('Seigan') > 0) then
        gSettings.SpellOffset = 4.0;
    else
        gSettings.SpellOffset = 3.0;
    end

    gFunc.EquipSet('Precast');
end

profile.HandleMidcast = function()
    local player = gData.GetPlayer();
    local action = gData.GetAction();
    local equipment = gData.GetEquipment();

    if (action.Skill == 'Dark Magic' and action.Name ~= 'Dread Spikes') then
        gFunc.EquipSet('Dark Magic');
    elseif (action.Skill == 'Enfeebling Magic') then
        gFunc.EquipSet('Enfeebling Magic');
    elseif (action.Skill == 'Elemental Magic') then
        main = equipment.Main and equipment.Main.Name or nil;
        sub = equipment.Sub and equipment.Sub.Name or nil;
        restore_weapons = true;

        gFunc.Equip('Main', common.staff_table[action.Element]);
        gFunc.Equip('Sub', action.Element .. ' Grip');
        gFunc.EquipSet('Elemental Magic');
    else
        gFunc.EquipSet('Haste');

        if (player.SubJob == 'DRG') then
            gFunc.Equip('Ear2', 'Wyvern Earring');
        end
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
    gFunc.EquipSet('Midshot');

    if (th_timer <= 20) then
        gFunc.Equip('Head', 'Wh. Rarab Cap +1');
    end
end

local ws_map = {
    ['Requiescat']      = 'Requiescat',
    ['Resolution']      = 'Resolution',
    ['Catastrophe']     = 'Catastrophe',
    ['Entropy']         = 'Entropy',
};

local gorget_map = {
    ['Requiescat']      = 'Soil Gorget',
    ['Resolution']      = 'Soil Gorget',
    ['Savage Blade']    = 'Soil Gorget',
    ['Vorpal Blade']    = 'Soil Gorget',
    ['Catastrophe']     = 'Soil Gorget',
    ['Entropy']         = 'Soil Gorget',
    ['Spiral Hell']     = 'Soil Gorget',
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();
    local environment = gData.GetEnvironment();

    gFunc.EquipSet('WS');

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end

    if (environment.Time >= 18 or environment.Time < 6) then
        if (action.Name ~= 'Entropy' and action.Name ~= 'Resolution') then
            gFunc.Equip('Ear1', 'Vampire Earring');
        end
        gFunc.Equip('Ear2', 'Vampire Earring');
    end

    common.RemoveSleep();
end

return profile;
