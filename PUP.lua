local profile = {};
local common = gFunc.LoadFile('common/common.lua');

local settings = {
    IdleVariant     = 'Default',
    TPVariant      = 'Default',
    WSetVariant     = 'Default',
    THMode          = true,
};

local sets = {
    ['Idle_Default'] = {
        Head    = 'Pantin Taj',
        Body    = 'Goliard Saio',
        Hands   = 'Pantin Dastanas',
        Legs    = 'Bahamut\'s Hose',
        Feet    = 'Pup. Babouches +1',
        Neck    = 'Orochi Nodowa',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Defending Ring',
        Back    = 'Shadow Mantle',
    },
    ['Idle_Alchemy'] = {
        Main    = 'Caduceus',
        Sub     = 'Kupo Shield',
        Head    = 'Pantin Taj',
        Body    = 'Alchemist\'s Apron',
        Hands   = 'Pantin Dastanas',
        Legs    = 'Bahamut\'s Hose',
        Feet    = 'Pup. Babouches +1',
        Neck    = 'Alchemst. Torque',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Warp Ring',
        Ring2   = 'Craftmaster\'s Ring',
        Back    = 'Shadow Mantle',
    },
    ['TP_Default'] = {
        Head    = 'Walahra Turban',
        Body    = 'Goliard Saio',
        Hands   = 'Pantin Dastanas',
        Legs    = 'Bahamut\'s Hose',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Pixie Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_Accuracy'] = {
        Head    = 'Walahra Turban',
        Body    = 'Goliard Saio',
        Hands   = 'Pantin Dastanas',
        Legs    = 'Bahamut\'s Hose',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Ancient Torque',
        Waist   = 'Speed Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Pixie Earring',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Sniper\'s Ring +1',
        Back    = 'Aesir Mantle',
    },
    ['TP_MDT'] = {
        Head    = 'Walahra Turban',
        Body    = 'Goliard Saio',
        Hands   = 'Pantin Dastanas',
        Legs    = 'Bahamut\'s Hose',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Caract Choker',
        Waist   = 'Lieutenant\'s Sash',
        Ear1    = 'Merman\'s Earring',
        Ear2    = 'Merman\'s Earring',
        Ring1   = 'Defending Ring',
        Ring2   = 'Succor Ring',
        Back    = 'Lamia Mantle +1',
    },
    ['WS_Default'] = {
        Head    = 'Voyager Sallet',
        Body    = 'Enkidu\'s Harness',
        Hands   = 'Enkidu\'s Mittens',
        Legs    = 'Bahamut\'s Hose',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Spike Necklace',
        Waist   = 'Virtuoso Belt',
        Ear1    = 'Brutal Earring',
        Ear2    = 'Bushinomimi',
        Ring1   = 'Rajas Ring',
        Ring2   = 'Flame Ring',
        Back    = 'Pantin Cape',
    },
    ['WS_Accuracy'] = {
        Head    = 'Htr. Beret +1',
        Body    = 'Kirin\'s Osode',
        Hands   = 'Seiryu\'s Kote',
        Legs    = 'Htr. Braccae +1',
        Feet    = 'Scout\'s Socks',
        Neck    = 'Hope Torque',
        Waist   = 'Scout\'s Belt',
        Ear1    = 'Drone Earring',
        Ear2    = 'Drone Earring',
        Ring1   = 'Emerald Ring',
        Ring2   = 'Emerald Ring',
        Back    = 'Amemet Mantle +1',
    },
    ['Haste'] = {
        Head    = 'Acubens Helm',
        Body    = 'Goliard Saio',
        Hands   = 'Pantin Dastanas',
        Feet    = 'Setanta\'s Led.',
        Neck    = 'Tiercel Necklace',
        Waist   = 'Swift Belt',
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
    },
    ['Precast'] = {
        Ear2    = 'Loquac. Earring',
        Ring1   = 'Naji\'s Loop',
        Back    = 'Veela Cape',
    },
};
profile.Sets = sets;

profile.Packer = {
};

profile.OnLoad = function()
    local macrobook = 15;

    gSettings.AllowAddSet = true;
    gSettings.AddSetEquipScreenOrder = false;
    AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro book %d', macrobook));
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /iset /lac fwd /iset');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /set /lac fwd /set');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /th /lac fwd /th');
    AshitaCore:GetChatManager():QueueCommand(-1, '/alias /thmode /lac fwd /thmode');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^a /ra <stnpc>');
    AshitaCore:GetChatManager():QueueCommand(-1, '/bind ^s /ra <t>');
end

profile.OnUnload = function()
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^a');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^s');
    AshitaCore:GetChatManager():QueueCommand(-1, '/unbind ^h');
end

local th_timer = 0;
local target_index = nil;

profile.HandleCommand = function(args)
    if (args[1] == '/set') then
        if (args[2] == 'acc') then
            settings.TPVariant = 'Accuracy';
        elseif (args[2] == 'def') then
            settings.TPVariant = 'Default';
        elseif (args[2] == 'mdt') then
            if (settings.TPVariant ~= 'MDT') then
                if (string.match(settings.TPVariant, '[PMB]DT') == nil) then
                    prev_set = settings.TPVariant;
                end
                settings.TPVariant = 'MDT';
            elseif (prev_set) then
                settings.TPVariant = prev_set;
            end
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('TP set variant: ' .. settings.TPVariant);
    elseif (args[1] == '/iset') then
        if (args[2] == 'def') then
            settings.IdleVariant = 'Default';
        elseif (args[2] == 'alc') then
            settings.IdleVariant = 'Alchemy';
        else
            gFunc.Error('Unknown set: ' .. tostring(args[2]));
            return;
        end
        gFunc.Message('Idle set variant: ' .. settings.IdleVariant);
    elseif (args[1] == '/th') then
        th_timer = 0;
    elseif (args[1] == '/thmode') then
        settings.THMode = not settings.THMode;
        gFunc.Message('TH Mode: ' .. tostring(settings.THMode));
    end
end

local subjob;
local counter = 0;
local macroset = 0;

profile.HandleDefault = function()
    local pet = gData.GetPet();
    local player = gData.GetPlayer();
    local target = gData.GetTarget();
    local equipment = gData.GetEquipment();

    if (counter <= 10) then
        counter = counter + 1;
    end
    if (counter == 10) then
        AshitaCore:GetChatManager():QueueCommand(1, '/lockstyleset 24');
        AshitaCore:GetChatManager():QueueCommand(1, string.format('/macro set %d', macroset));
    end

    if (subjob == nil and player.SubJob ~= 'NON') then
        subjob = player.SubJob;
        macroset = (player.SubJob == 'WHM') and 2
                    (player.SubJob == 'RDM') and 2 or 2;
    end
    if (subjob ~= nil and player.SubJob ~= 'NON' and player.SubJob ~= subjob) then
        AshitaCore:GetChatManager():QueueCommand(-1, '/lac reload');
    end

    if (target ~= nil and target.Index ~= target_index) then
        target_index = target.Index;
        th_timer = 0;
    end

    if (player.Status == 'Engaged') then
        gFunc.EquipSet('TP_' .. settings.TPVariant);

        if (pet ~= nil and pet.HPP < 70) then
            gFunc.Equip('Feet', 'Pup. Babouches +1');
        end

        if (player.SubJob == 'DRG') then
            gFunc.Equip('Ear2', 'Wyvern Earring');
        end

        if (th_timer <= 20 and settings.THMode) then
            th_timer = th_timer + 1;
            gFunc.Equip('Head', 'Wh. Rarab Cap +1');
        end

        common.RemoveSleep();
    else
        gFunc.EquipSet('Idle_' .. settings.IdleVariant);
    end

    if (player.IsMoving) then
        gFunc.Equip('Feet', 'Hermes\' Sandals');
    end

    common.CheckManualEquips();
end

profile.HandleAbility = function()
    local action = gData.GetAction();

    if (string.match(action.Name, 'Maneuver')) then
        gFunc.Equip('Hands', 'Pup. Dastanas');
        gFunc.Equip('Neck', 'Bfn. Collar +1');
    elseif (action.Name == 'Repair') then
        gFunc.Equip('Feet', 'Pup. Babouches +1');
    end
end

profile.HandleItem = function()
end

profile.HandlePrecast = function()
    gFunc.EquipSet('Precast');
end

profile.HandleMidcast = function()
    local player = gData.GetPlayer();

    gFunc.EquipSet('Haste');

    if (player.SubJob == 'DRG') then
        gFunc.Equip('Ear2', 'Wyvern Earring');
    end
end

profile.HandlePreshot = function()
end

profile.HandleMidshot = function()
end

local ws_map = {
};

local gorget_map = {
};

profile.HandleWeaponskill = function()
    local action = gData.GetAction();
    local equipment = gData.GetEquipment();
    local environment = gData.GetEnvironment();

    gFunc.EquipSet('WS_' .. settings.WSetVariant);

    if (ws_map[action.Name] ~= nil) then
        gFunc.EquipSet(ws_map[action.Name]);
    end

    if (gorget_map[action.Name] ~= nil) then
        gFunc.Equip('Neck', gorget_map[action.Name]);
    end

    common.RemoveSleep();
end

return profile;
